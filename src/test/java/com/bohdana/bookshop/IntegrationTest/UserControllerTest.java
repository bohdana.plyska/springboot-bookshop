//package com.bohdana.bookshop.IntegrationTest;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//@WithMockUser(username = "k", password = "11111111")
//public class UserControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    public void saveUserTest() throws Exception{
//        mockMvc.perform(post("/save").param("username", "Vlad").param("password", "11111111").param("email", "vlad@gmail.comm"))
//                .andDo(print())
//                .andExpect(authenticated())
//                .andExpect(status().is2xxSuccessful())
//                .andExpect(model().attribute("Success", true));
//    }
//
//}
