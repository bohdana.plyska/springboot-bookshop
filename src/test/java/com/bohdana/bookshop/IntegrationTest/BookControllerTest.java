//package com.bohdana.bookshop.IntegrationTest;
//
//import org.junit.Test;
//import org.junit.jupiter.api.MethodOrderer;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import static org.hamcrest.Matchers.containsString;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//
//
//@SpringBootTest
//@AutoConfigureMockMvc
//@WithMockUser(username = "bohdashka", password = "11111111")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//public class BookControllerTest {
//
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    public void shouldReturnDefaultMessage() throws Exception {
//        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
//                .andExpect(content().string(containsString("Hello, World")));
//    }
//
////    @Test
////    @Order(1)
////    public void saveAndUpdateBook() throws Exception {
////        this.mockMvc.perform(post("/saveBook").param("name", "Second").param("publisherName", "test description")
////                        .param("authorName", "Emily").param("title", "Second").param("price", "100"))
////                .andDo(print())
////                .andExpect(authenticated())
////                .andExpect(status().is2xxSuccessful())
////                .andExpect(model().attribute("SuccessSaveBook", true));
////    }
//}
