package com.bohdana.bookshop.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.Role;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.ContentResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {UserController.class})
@ExtendWith(SpringExtension.class)
class UserControllerTest {
    @Autowired
    private UserController userController;

    @MockBean
    private UserService userService;

    /**
     * Method under test: {@link UserController#deleteUser(Long)}
     */
    @Test
    void testDeleteUser() throws Exception {
        User user = new User();
        user.setActive(true);
        user.setBirthDate(LocalDate.ofEpochDay(1L));
        user.setCity("Oxford");
        user.setCountry("GB");
        user.setDistrict("District");
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setLastName("Doe");
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPasswordConfirm("Password Confirm");
        user.setRole(Role.ROLE_USER);
        user.setSalt("Salt");
        user.setStreetAddress("42 Main St");
        user.setUsername("janedoe");
        user.setZipcode("21654");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.getUser((Long) any())).thenReturn(ofResult);
        doNothing().when(this.userService).delete((Long) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/users/delete/{id}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        ResultActions resultActions = actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.FALSE.toString()));
    }

    /**
     * Method under test: {@link UserController#findById(Long)}
     */
    @Test
    void testFindById() throws Exception {
        when(this.userService.findById((Long) any())).thenReturn(new UserDTO());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/{id}", 123L);
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":null,\"username\":null,\"password\":null,\"passwordConfirm\":null,\"email\":null,\"city\":null,\"country\""
                                        + ":null,\"district\":null,\"streetAddress\":null,\"zipcode\":null,\"birthDate\":null,\"salt\":null,\"role\":null}"));
    }

    /**
     * Method under test: {@link UserController#findById(Long)}
     */
    @Test
    void testFindById2() throws Exception {
        when(this.userService.findById((Long) any())).thenReturn(new UserDTO());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/users/{id}", 123L);
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":null,\"username\":null,\"password\":null,\"passwordConfirm\":null,\"email\":null,\"city\":null,\"country\""
                                        + ":null,\"district\":null,\"streetAddress\":null,\"zipcode\":null,\"birthDate\":null,\"salt\":null,\"role\":null}"));
    }

    /**
     * Method under test: {@link UserController#findById(Long)}
     */
    @Test
    void testFindById3() throws Exception {
        when(this.userService.listAllUsers()).thenReturn(new ArrayList<>());
        when(this.userService.findById((Long) any())).thenReturn(new UserDTO());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/{id}", "", "Uri Vars");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link UserController#deleteUser(Long)}
     */
    @Test
    void testDeleteUser2() throws Exception {
        when(this.userService.getUser((Long) any())).thenReturn(Optional.empty());
        doNothing().when(this.userService).delete((Long) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/users/delete/{id}", 123L);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link UserController#deleteUser(Long)}
     */
    @Test
    void testDeleteUser3() throws Exception {
        User user = new User();
        user.setActive(true);
        user.setBirthDate(LocalDate.ofEpochDay(1L));
        user.setCity("Oxford");
        user.setCountry("GB");
        user.setDistrict("District");
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setLastName("Doe");
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPasswordConfirm("Password Confirm");
        user.setRole(Role.ROLE_USER);
        user.setSalt("Salt");
        user.setStreetAddress("42 Main St");
        user.setUsername("janedoe");
        user.setZipcode("21654");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.getUser((Long) any())).thenReturn(ofResult);
        doNothing().when(this.userService).delete((Long) any());
        MockHttpServletRequestBuilder deleteResult = MockMvcRequestBuilders.delete("/users/delete/{id}", 123L);
        deleteResult.contentType("https://example.org/example");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(deleteResult);
        ResultActions resultActions = actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.FALSE.toString()));
    }

    /**
     * Method under test: {@link UserController#findByUserName(String)}
     */
    @Test
    void testFindByUserName() throws Exception {
        when(this.userService.findByUsername((String) any())).thenReturn(new UserDTO());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/findByUserName/{userName}",
                "janedoe");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":null,\"username\":null,\"password\":null,\"passwordConfirm\":null,\"email\":null,\"city\":null,\"country\""
                                        + ":null,\"district\":null,\"streetAddress\":null,\"zipcode\":null,\"birthDate\":null,\"salt\":null,\"role\":null}"));
    }

    /**
     * Method under test: {@link UserController#findByUserName(String)}
     */
    @Test
    void testFindByUserName2() throws Exception {
        when(this.userService.findByUsername((String) any())).thenReturn(new UserDTO());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/users/findByUserName/{userName}", "janedoe");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":null,\"username\":null,\"password\":null,\"passwordConfirm\":null,\"email\":null,\"city\":null,\"country\""
                                        + ":null,\"district\":null,\"streetAddress\":null,\"zipcode\":null,\"birthDate\":null,\"salt\":null,\"role\":null}"));
    }

    /**
     * Method under test: {@link UserController#saveUser(UserDTO)}
     */
    @Test
    void testSaveUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setBirthDate(LocalDate.ofEpochDay(1L));
        userDTO.setCity("Oxford");
        userDTO.setCountry("GB");
        userDTO.setDistrict("District");
        userDTO.setEmail("jane.doe@example.org");
        userDTO.setId(123L);
        userDTO.setPassword("iloveyou");
        userDTO.setPasswordConfirm("Password Confirm");
        userDTO.setRole(Role.ROLE_USER);
        userDTO.setSalt("Salt");
        userDTO.setStreetAddress("42 Main St");
        userDTO.setUsername("janedoe");
        userDTO.setZipcode("21654");
        String content = (new ObjectMapper()).writeValueAsString(userDTO);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link UserController#saveUser(UserDTO)}
     */
    @Test
    void testSaveUser2() throws Exception {
        User user = new User();
        user.setActive(true);
        user.setBirthDate(LocalDate.ofEpochDay(1L));
        user.setCity("Oxford");
        user.setCountry("GB");
        user.setDistrict("District");
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setLastName("Doe");
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPasswordConfirm("Password Confirm");
        user.setRole(Role.ROLE_USER);
        user.setSalt("Salt");
        user.setStreetAddress("42 Main St");
        user.setUsername("janedoe");
        user.setZipcode("21654");
        when(this.userService.save((UserDTO) any())).thenReturn(user);

        UserDTO userDTO = new UserDTO();
        userDTO.setBirthDate(null);
        userDTO.setCity("Oxford");
        userDTO.setCountry("GB");
        userDTO.setDistrict("District");
        userDTO.setEmail("jane.doe@example.org");
        userDTO.setId(123L);
        userDTO.setPassword("iloveyou");
        userDTO.setPasswordConfirm("Password Confirm");
        userDTO.setRole(Role.ROLE_USER);
        userDTO.setSalt("Salt");
        userDTO.setStreetAddress("42 Main St");
        userDTO.setUsername("janedoe");
        userDTO.setZipcode("21654");
        String content = (new ObjectMapper()).writeValueAsString(userDTO);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"username\":\"janedoe\",\"name\":\"Name\",\"lastName\":\"Doe\",\"email\":\"jane.doe@example.org\",\"password"
                                        + "\":\"iloveyou\",\"passwordConfirm\":\"Password Confirm\",\"city\":\"Oxford\",\"country\":\"GB\",\"district\":\"District"
                                        + "\",\"streetAddress\":\"42 Main St\",\"zipcode\":\"21654\",\"birthDate\":[1970,1,2],\"active\":true,\"salt\":\"Salt\","
                                        + "\"role\":\"ROLE_USER\"}"));
    }

    /**
     * Method under test: {@link UserController#searchUser(String)}
     */
    @Test
    void testSearchUser() throws Exception {
        when(this.userService.search((String) any())).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/searchUser/{keyword}", "Keyword");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link UserController#searchUser(String)}
     */
    @Test
    void testSearchUser2() throws Exception {
        when(this.userService.search((String) any())).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/searchUser/{keyword}", "Keyword");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link UserController#showAllUsers()}
     */
    @Test
    void testShowAllUsers() throws Exception {
        when(this.userService.listAllUsers()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link UserController#showAllUsers()}
     */
    @Test
    void testShowAllUsers2() throws Exception {
        when(this.userService.listAllUsers()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/users");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link UserController#updateUser(UserDTO)}
     */
    @Test
    void testUpdateUser() throws Exception {
        User user = new User();
        user.setActive(true);
        user.setBirthDate(LocalDate.ofEpochDay(1L));
        user.setCity("Oxford");
        user.setCountry("GB");
        user.setDistrict("District");
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setLastName("Doe");
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPasswordConfirm("Password Confirm");
        user.setRole(Role.ROLE_USER);
        user.setSalt("Salt");
        user.setStreetAddress("42 Main St");
        user.setUsername("janedoe");
        user.setZipcode("21654");
        when(this.userService.update((UserDTO) any())).thenReturn(user);
        MockHttpServletRequestBuilder postResult = MockMvcRequestBuilders.post("/users/edit");
        MockHttpServletRequestBuilder requestBuilder = postResult.param("userDTO", String.valueOf(new UserDTO()));
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"username\":\"janedoe\",\"name\":\"Name\",\"lastName\":\"Doe\",\"email\":\"jane.doe@example.org\",\"password"
                                        + "\":\"iloveyou\",\"passwordConfirm\":\"Password Confirm\",\"city\":\"Oxford\",\"country\":\"GB\",\"district\":\"District"
                                        + "\",\"streetAddress\":\"42 Main St\",\"zipcode\":\"21654\",\"birthDate\":[1970,1,2],\"active\":true,\"salt\":\"Salt\","
                                        + "\"role\":\"ROLE_USER\"}"));
    }
}

