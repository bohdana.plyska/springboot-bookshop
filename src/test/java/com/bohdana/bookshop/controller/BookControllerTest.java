package com.bohdana.bookshop.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.GenreEnum;
import com.bohdana.bookshop.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.ContentResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {BookController.class})
@ExtendWith(SpringExtension.class)
class BookControllerTest {
    @Autowired
    private BookController bookController;

    @MockBean
    private BookService bookService;

    /**
     * Method under test: {@link BookController#addGenresToModel()}
     */
    @Test
    void testAddGenresToModel() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/genres");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string("[\"Fantasy\",\"Adventure\",\"Romance\",\"Mystery\",\"Horror\",\"Thriller\",\"Paranormal\"]"));
    }

    /**
     * Method under test: {@link BookController#findAllAvailable()}
     */
    @Test
    void testFindAllAvailable() throws Exception {
        when(this.bookService.findAllAvailable()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/findAllAvailable");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link BookController#findAllAvailable()}
     */
    @Test
    void testFindAllAvailable2() throws Exception {
        when(this.bookService.findAllAvailable()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/findAllAvailable");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link BookController#findById(Long)}
     */
    @Test
    void testFindById() throws Exception {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setAmount(10L);
        bookDTO.setAuthorName("JaneDoe");
        bookDTO.setBookType("Book Type");
        bookDTO.setId(123L);
        bookDTO.setName("Name");
        bookDTO.setNumberOfPages(1L);
        bookDTO.setPrice(10.0f);
        bookDTO.setPublisherName("Publisher Name");
        bookDTO.setTitle("Dr");
        when(this.bookService.findById((Long) any())).thenReturn(bookDTO);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/books/{id}", 123L);
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"name\":\"Name\",\"publisherName\":\"Publisher Name\",\"authorName\":\"JaneDoe\",\"title\":\"Dr\",\"price\""
                                        + ":10.0,\"numberOfPages\":1,\"amount\":10,\"bookType\":\"Book Type\"}"));
    }

    /**
     * Method under test: {@link BookController#findById(Long)}
     */
    @Test
    void testFindById2() throws Exception {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setAmount(10L);
        bookDTO.setAuthorName("JaneDoe");
        bookDTO.setBookType("Book Type");
        bookDTO.setId(123L);
        bookDTO.setName("Name");
        bookDTO.setNumberOfPages(1L);
        bookDTO.setPrice(10.0f);
        bookDTO.setPublisherName("Publisher Name");
        bookDTO.setTitle("Dr");
        when(this.bookService.findById((Long) any())).thenReturn(bookDTO);
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/books/{id}", 123L);
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"name\":\"Name\",\"publisherName\":\"Publisher Name\",\"authorName\":\"JaneDoe\",\"title\":\"Dr\",\"price\""
                                        + ":10.0,\"numberOfPages\":1,\"amount\":10,\"bookType\":\"Book Type\"}"));
    }

    /**
     * Method under test: {@link BookController#addGenresToModel()}
     */
    @Test
    void testAddGenresToModel2() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/genres", "Uri Vars");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string("[\"Fantasy\",\"Adventure\",\"Romance\",\"Mystery\",\"Horror\",\"Thriller\",\"Paranormal\"]"));
    }

    /**
     * Method under test: {@link BookController#deleteBook(Long)}
     */
    @Test
    void testDeleteBook() throws Exception {
        Book book = new Book();
        book.setAmount(10L);
        book.setAuthorName("JaneDoe");
        book.setBookType(GenreEnum.Fantasy);
        book.setId(123L);
        book.setName("Name");
        book.setNumberOfPages(1L);
        book.setPrice(10.0f);
        book.setPublisherName("Publisher Name");
        book.setTitle("Dr");
        Optional<Book> ofResult = Optional.of(book);
        when(this.bookService.getBook((Long) any())).thenReturn(ofResult);
        doNothing().when(this.bookService).delete((Long) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/books/delete/{id}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder);
        ResultActions resultActions = actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.FALSE.toString()));
    }

    /**
     * Method under test: {@link BookController#deleteBook(Long)}
     */
    @Test
    void testDeleteBook2() throws Exception {
        when(this.bookService.getBook((Long) any())).thenReturn(Optional.empty());
        doNothing().when(this.bookService).delete((Long) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/books/delete/{id}", 123L);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link BookController#deleteBook(Long)}
     */
    @Test
    void testDeleteBook3() throws Exception {
        Book book = new Book();
        book.setAmount(10L);
        book.setAuthorName("JaneDoe");
        book.setBookType(GenreEnum.Fantasy);
        book.setId(123L);
        book.setName("Name");
        book.setNumberOfPages(1L);
        book.setPrice(10.0f);
        book.setPublisherName("Publisher Name");
        book.setTitle("Dr");
        Optional<Book> ofResult = Optional.of(book);
        when(this.bookService.getBook((Long) any())).thenReturn(ofResult);
        doNothing().when(this.bookService).delete((Long) any());
        MockHttpServletRequestBuilder deleteResult = MockMvcRequestBuilders.delete("/books/delete/{id}", 123L);
        deleteResult.contentType("https://example.org/example");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(deleteResult);
        ResultActions resultActions = actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.FALSE.toString()));
    }

    /**
     * Method under test: {@link BookController#saveAndUpdateBook(Book)}
     */
    @Test
    void testSaveAndUpdateBook() throws Exception {
        Book book = new Book();
        book.setAmount(10L);
        book.setAuthorName("JaneDoe");
        book.setBookType(GenreEnum.Fantasy);
        book.setId(123L);
        book.setName("Name");
        book.setNumberOfPages(1L);
        book.setPrice(10.0f);
        book.setPublisherName("Publisher Name");
        book.setTitle("Dr");
        when(this.bookService.save((Book) any())).thenReturn(book);

        Book book1 = new Book();
        book1.setAmount(10L);
        book1.setAuthorName("JaneDoe");
        book1.setBookType(GenreEnum.Fantasy);
        book1.setId(123L);
        book1.setName("Name");
        book1.setNumberOfPages(1L);
        book1.setPrice(10.0f);
        book1.setPublisherName("Publisher Name");
        book1.setTitle("Dr");
        String content = (new ObjectMapper()).writeValueAsString(book1);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/saveBook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"name\":\"Name\",\"publisherName\":\"Publisher Name\",\"authorName\":\"JaneDoe\",\"title\":\"Dr\",\"price\""
                                        + ":10.0,\"numberOfPages\":1,\"amount\":10,\"bookType\":\"Fantasy\"}"));
    }

    /**
     * Method under test: {@link BookController#searchBook(String)}
     */
    @Test
    void testSearchBook() throws Exception {
        when(this.bookService.search((String) any())).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/searchBook/{keyword}", "Keyword");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link BookController#searchBook(String)}
     */
    @Test
    void testSearchBook2() throws Exception {
        when(this.bookService.search((String) any())).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/searchBook/{keyword}", "Keyword");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link BookController#showAllBooks()}
     */
    @Test
    void testShowAllBooks() throws Exception {
        when(this.bookService.listAllBooks()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/books");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link BookController#showAllBooks()}
     */
    @Test
    void testShowAllBooks2() throws Exception {
        when(this.bookService.listAllBooks()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/books");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.bookController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }
}

