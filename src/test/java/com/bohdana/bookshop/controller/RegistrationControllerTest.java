package com.bohdana.bookshop.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.Role;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {RegistrationController.class})
@ExtendWith(SpringExtension.class)
class RegistrationControllerTest {
    @Autowired
    private RegistrationController registrationController;

    @MockBean
    private UserService userService;

    /**
     * Method under test: {@link RegistrationController#signUpUser(UserDTO)}
     */
    @Test
    void testSignUpUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setBirthDate(LocalDate.ofEpochDay(1L));
        userDTO.setCity("Oxford");
        userDTO.setCountry("GB");
        userDTO.setDistrict("District");
        userDTO.setEmail("jane.doe@example.org");
        userDTO.setId(123L);
        userDTO.setPassword("iloveyou");
        userDTO.setPasswordConfirm("Password Confirm");
        userDTO.setRole(Role.ROLE_USER);
        userDTO.setSalt("Salt");
        userDTO.setStreetAddress("42 Main St");
        userDTO.setUsername("janedoe");
        userDTO.setZipcode("21654");
        String content = (new ObjectMapper()).writeValueAsString(userDTO);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.registrationController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link RegistrationController#signUpUser(UserDTO)}
     */
    @Test
    void testSignUpUser2() throws Exception {
        User user = new User();
        user.setActive(true);
        user.setBirthDate(LocalDate.ofEpochDay(1L));
        user.setCity("Oxford");
        user.setCountry("GB");
        user.setDistrict("District");
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setLastName("Doe");
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPasswordConfirm("Password Confirm");
        user.setRole(Role.ROLE_USER);
        user.setSalt("Salt");
        user.setStreetAddress("42 Main St");
        user.setUsername("janedoe");
        user.setZipcode("21654");

        User user1 = new User();
        user1.setActive(true);
        user1.setBirthDate(LocalDate.ofEpochDay(1L));
        user1.setCity("Oxford");
        user1.setCountry("GB");
        user1.setDistrict("District");
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setLastName("Doe");
        user1.setName("Name");
        user1.setPassword("iloveyou");
        user1.setPasswordConfirm("Password Confirm");
        user1.setRole(Role.ROLE_USER);
        user1.setSalt("Salt");
        user1.setStreetAddress("42 Main St");
        user1.setUsername("janedoe");
        user1.setZipcode("21654");
        when(this.userService.getUserByName((String) any())).thenReturn(user);
        when(this.userService.save((UserDTO) any())).thenReturn(user1);

        UserDTO userDTO = new UserDTO();
        userDTO.setBirthDate(null);
        userDTO.setCity("Oxford");
        userDTO.setCountry("GB");
        userDTO.setDistrict("District");
        userDTO.setEmail("jane.doe@example.org");
        userDTO.setId(123L);
        userDTO.setPassword("iloveyou");
        userDTO.setPasswordConfirm("Password Confirm");
        userDTO.setRole(Role.ROLE_USER);
        userDTO.setSalt("Salt");
        userDTO.setStreetAddress("42 Main St");
        userDTO.setUsername("janedoe");
        userDTO.setZipcode("21654");
        String content = (new ObjectMapper()).writeValueAsString(userDTO);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.registrationController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"username\":\"janedoe\",\"password\":\"iloveyou\",\"passwordConfirm\":\"Password Confirm\",\"email\":"
                                        + "\"jane.doe@example.org\",\"city\":\"Oxford\",\"country\":\"GB\",\"district\":\"District\",\"streetAddress\":\"42 Main"
                                        + " St\",\"zipcode\":\"21654\",\"birthDate\":[1970,1,2],\"salt\":\"Salt\",\"role\":\"ROLE_USER\"}"));
    }
}

