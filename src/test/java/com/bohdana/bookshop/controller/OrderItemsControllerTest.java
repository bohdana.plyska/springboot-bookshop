package com.bohdana.bookshop.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.bohdana.bookshop.service.OrderItemsService;
import com.bohdana.bookshop.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.ContentResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {OrderItemsController.class})
@ExtendWith(SpringExtension.class)
class OrderItemsControllerTest {
    @Autowired
    private OrderItemsController orderItemsController;

    @MockBean
    private OrderItemsService orderItemsService;

    @MockBean
    private UserService userService;

    /**
     * Method under test: {@link OrderItemsController#increaseAmount(Long)}
     */
    @Test
    void testIncreaseAmount() throws Exception {
        when(this.orderItemsService.increaseAmount((Long) any())).thenReturn(1);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/increaseAmount/{id}", 123L);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link OrderItemsController#increaseAmount(Long)}
     */
    @Test
    void testIncreaseAmount2() throws Exception {
        when(this.orderItemsService.increaseAmount((Long) any())).thenReturn(1);
        MockHttpServletRequestBuilder postResult = MockMvcRequestBuilders.post("/increaseAmount/{id}", 123L);
        postResult.contentType("https://example.org/example");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(postResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link OrderItemsController#addOrderItemsToOrder(Long, org.springframework.security.core.Authentication)}
     */
    @Test
    void testAddOrderItemsToOrder() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/addToBasket/{bookId}", "Uri Vars",
                "Uri Vars");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link OrderItemsController#decreaseAmount(Long)}
     */
    @Test
    void testDecreaseAmount() throws Exception {
        when(this.orderItemsService.decreaseAmount((Long) any())).thenReturn(1);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/decreaseAmount/{id}", 123L);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link OrderItemsController#decreaseAmount(Long)}
     */
    @Test
    void testDecreaseAmount2() throws Exception {
        when(this.orderItemsService.decreaseAmount((Long) any())).thenReturn(1);
        MockHttpServletRequestBuilder postResult = MockMvcRequestBuilders.post("/decreaseAmount/{id}", 123L);
        postResult.contentType("https://example.org/example");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(postResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link OrderItemsController#delete(Long)}
     */
    @Test
    void testDelete() throws Exception {
        doNothing().when(this.orderItemsService).delete((Long) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/deleteOrderItem/{id}", 123L);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link OrderItemsController#delete(Long)}
     */
    @Test
    void testDelete2() throws Exception {
        doNothing().when(this.orderItemsService).delete((Long) any());
        MockHttpServletRequestBuilder deleteResult = MockMvcRequestBuilders.delete("/deleteOrderItem/{id}", 123L);
        deleteResult.contentType("https://example.org/example");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(deleteResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"));
        ContentResultMatchers contentResult = MockMvcResultMatchers.content();
        resultActions.andExpect(contentResult.string(Boolean.TRUE.toString()));
    }

    /**
     * Method under test: {@link OrderItemsController#showBasket(org.springframework.security.core.Authentication)}
     */
    @Test
    void testShowBasket() throws Exception {
        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder requestBuilder = SecurityMockMvcRequestBuilders.formLogin();
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.orderItemsController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}

