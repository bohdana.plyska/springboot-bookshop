package com.bohdana.bookshop.ServicesTest;

import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.GenreEnum;
import com.bohdana.bookshop.mappers.BookMapper;
import com.bohdana.bookshop.repository.BookRepository;
import com.bohdana.bookshop.service.impl.BookServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceUnitTest {

    @InjectMocks
    private BookServiceImpl bookService;

    @Mock
    private BookRepository bookRepository;

    @Test
    public void saveBook(){
        Book bookDTO = new Book();
        bookDTO.setId(1L);
        bookDTO.setName("book");
        bookDTO.setPublisherName("Katy");
        bookDTO.setAuthorName("Andriy");
        bookDTO.setTitle("book");
        bookDTO.setPrice(123.0F);
        bookDTO.setNumberOfPages(100L);
        bookDTO.setAmount(10L);
        bookDTO.setBookType(GenreEnum.Adventure);
        when(bookRepository.save(any(Book.class))).thenReturn(new Book(1L,"book","Katy",
                "Andriy","book", 123.0F,100L,10L, GenreEnum.Adventure));
        Book created = bookService.save(bookDTO);
         assertEquals(created.getName(), bookDTO.getName());
    }
    @Test
    public void deleteBook(){
        Book book = new Book(1L,"book","Katy",
                "Andriy","book", 123.0F,100L,10L, GenreEnum.Adventure);
        when(bookRepository.findById(book.getId())).thenReturn(Optional.of(book));
        bookService.delete(book.getId());
        verify(bookRepository,times(1)).delete(book);
    }

    @Test
    public void updateBook(){
        BookDTO bookDTO = new BookDTO();
        bookDTO.setId(1L);
        bookDTO.setName("book");
        bookDTO.setPublisherName("Katy");
        bookDTO.setAuthorName("Andriy");
        bookDTO.setTitle("book");
        bookDTO.setPrice(123.0F);
        bookDTO.setNumberOfPages(100L);
        bookDTO.setAmount(10L);
        bookDTO.setBookType("Adventure");
        bookService.update(bookDTO);
        verify(bookRepository,times(1)).save(BookMapper.INSTANCE.bookDtoToBook(bookDTO));
    }

    @Test
    public void listBooks(){
        List<Book> list = new ArrayList<>();
        Book book1 = new Book(1L,"book1","Katy",
                "Andriy","book1", 123.0F,100L,10L, GenreEnum.Adventure);
        Book book2 = new Book(2L,"book2","Nate",
                "Anna","book2", 120.0F,10L,100L, GenreEnum.Fantasy);

        list.add(book1);
        list.add(book2);
        when(bookRepository.findAll()).thenReturn(list);

        List<Book> bookList = bookService.listAllBooks();

        assertEquals(2,bookList.size());
        verify(bookRepository,times(1)).findAll();
    }

}
