package com.bohdana.bookshop.ServicesTest;
import com.bohdana.bookshop.dto.OrderDTO;
import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.*;
import com.bohdana.bookshop.mappers.UserMapper;
import com.bohdana.bookshop.repository.OrderItemsRepository;
import com.bohdana.bookshop.repository.OrderRepository;
import com.bohdana.bookshop.service.BookService;
import com.bohdana.bookshop.service.UserService;
import com.bohdana.bookshop.service.impl.OrderServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class OrderServiceUnitTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderItemsRepository orderItemsRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private UserService userService;

    @Mock
    private BookService bookService;

    private static final double DELTA = 1e-15;

     @Test
    public void saveOrder(){
         User user = new User(1L, "user","password");
         UserDTO userDTO = UserMapper.INSTANCE.userToUserDto(user);

         Book book = new Book(1L,"book","Katy",
                 "Andriy","book", 2F,100L,2L, GenreEnum.Adventure);

         Book book2 = new Book(2L,"book2","Nate",
                 "Anna","book2", 1F,10L,1L, GenreEnum.Adventure);

         OrderItems item = new OrderItems();
         item.setId(1l);
         item.setBookId(book);
         item.setPinned(false);
         item.setPrice(book.getPrice());
         item.setBookAmount(2);
         item.setUserId(user);

         OrderItems item2 = new OrderItems();
         item2.setId(2l);
         item2.setBookId(book2);
         item2.setPinned(false);
         item2.setPrice(book2.getPrice());
         item2.setBookAmount(1);
         item2.setUserId(user);


         List<OrderItems> list = new ArrayList<>();
         list.add(item);
         list.add(item2);


         double expectedPrice = 0.0;

         for (OrderItems orderItem: list) {
             expectedPrice = expectedPrice + orderItem.getPrice() * orderItem.getBookAmount();
             System.out.println(orderItem);
         }

         Order order = new Order();
         order.setUser(user);
         order.setOrderItems(list);
         order.setPrice(expectedPrice);

         when(userService.findById(user.getId())).thenReturn(userDTO);
         when(orderItemsRepository.findUnpinnedUserOrderDetails(user.getId())).thenReturn(list);
         when(bookService.updateBookAmount(item.getBookId().getId(),item.getBookAmount())).thenReturn(true);
         when(orderItemsRepository.save(any(OrderItems.class))).thenReturn(item);
         when(orderRepository.save(any(Order.class))).thenReturn(order);

         OrderDTO orderDTO = orderService.createOrder(user.getId());

         assertEquals(orderDTO.getPrice(),order.getPrice(),DELTA);
     }

    @Test
    public void getOrdersByUserId(){
         User user = new User(1L,"user","password");

         List<Order> list = new ArrayList<>();

         Order order1 = new Order(1l,20.2,false,user);
         Order order2 = new Order(2l,30.2,false,user);
         Order order3 = new Order(3l,56.2,false,user);

         list.add(order1);
         list.add(order2);
         list.add(order3);

         when(orderRepository.getOrdersByUserId(user.getId())).thenReturn(list);

         List<Order> orderList = orderService.getOrdersByUserId(user.getId());

         assertEquals(3,orderList.size());
         verify(orderRepository, times(1)).getOrdersByUserId(user.getId());
     }

}
