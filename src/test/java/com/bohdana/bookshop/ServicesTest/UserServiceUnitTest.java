package com.bohdana.bookshop.ServicesTest;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.mappers.UserMapper;
import com.bohdana.bookshop.repository.UserRepository;
import com.bohdana.bookshop.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class UserServiceUnitTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void saveUser(){
        UserDTO userDTO = new UserDTO(1L, "user", null);
        when(userRepository.save(any(User.class))).thenReturn(new User(1L, "user", null));
        User created = userService.save(userDTO);
        assertEquals(created.getUsername(), userDTO.getUsername());
    }

    @Test
    public void deleteUser(){
        User user = new User(1L, "user1", "password1");
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        userService.delete(user.getId());
        verify(userRepository, times(1)).delete(user);
    }

    @Test
    public void updateUser(){
        UserDTO userDTO = new UserDTO(1L, "user1", "password1");
        userService.update(userDTO);
        verify(userRepository, times(1)).save(UserMapper.INSTANCE.userDtoToUser(userDTO));
    }

    @Test
    public void listUsers(){
        List<User> list = new ArrayList<>();
        User user1 = new User(1L, "user1", "password1");
        User user2 = new User(2L, "user2", "password2");
        User user3 = new User(3L, "user3", "password3");

        list.add(user1);
        list.add(user2);
        list.add(user3);

        when(userRepository.findAll()).thenReturn(list);

        List<User> userList = userService.listAllUsers();

        assertEquals(3, userList.size());
        verify(userRepository, times(1)).findAll();
    }

}
