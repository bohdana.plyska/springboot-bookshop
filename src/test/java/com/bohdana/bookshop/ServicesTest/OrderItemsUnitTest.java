package com.bohdana.bookshop.ServicesTest;

import com.bohdana.bookshop.dto.OrderItemsDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.GenreEnum;
import com.bohdana.bookshop.entity.OrderItems;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.repository.BookRepository;
import com.bohdana.bookshop.repository.OrderItemsRepository;
import com.bohdana.bookshop.repository.UserRepository;
import com.bohdana.bookshop.service.impl.OrderItemsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderItemsUnitTest {

    @InjectMocks
    private OrderItemsServiceImpl orderItemsService;

    @Mock
    private OrderItemsRepository orderItemsRepository;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private UserRepository userRepository;

    @Test
    public void addOrderItems(){
        User user = new User(1L,"user","password");

        Book book = new Book(1L,"book","Katy", "Andriy","book", 123.0F,100L,10L, GenreEnum.Adventure);

        OrderItemsDTO orderItems= new OrderItemsDTO(book.getId(),user.getId());

        when(bookRepository.findById(book.getId())).thenReturn(Optional.of(book));

        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        when(orderItemsRepository.save(any(OrderItems.class))).thenReturn(new OrderItems(book,user));

        OrderItems created = orderItemsService.addOrderItems(orderItems);

        assertEquals(created.getBookId().getId(), orderItems.getBookId());
        assertEquals(created.getUserId().getId(), orderItems.getUserId());

    }

    @Test
    public void deleteOrderItems(){
        OrderItems orderItems = new OrderItems(1L,10,100F, false);
        when(orderItemsRepository.findById(orderItems.getId())).thenReturn(Optional.of(orderItems));

        orderItemsService.delete(orderItems.getId());
        verify(orderItemsRepository,times(1)).deleteById(orderItems.getId());
    }

    @Test
    public void getOrderItemsForBasket(){
        User user = new User(1L,"user","password");
        Book book1 = new Book(1L,"book","Katy", "Andriy","book", 123.0F,100L,10L, GenreEnum.Adventure);
        Book book2 = new Book(2L,"book","Katy", "Andriy","book", 123.0F,100L,10L, GenreEnum.Adventure);

        OrderItems orderItems1 = new OrderItems(book1,user);
        OrderItems orderItems2 = new OrderItems(book2,user);

        List<OrderItems> list = new ArrayList<>();

        list.add(orderItems1);
        list.add(orderItems2);

        when(orderItemsRepository.getOrderItemsForBasket(user.getId())).thenReturn(list);

        List<OrderItems> result = orderItemsService.getOrderItemsForBasket(user.getId());
        assertEquals(2,result.size());
        verify(orderItemsRepository, times(1)).getOrderItemsForBasket(user.getId());


    }

    @Test
    public void updateAmount(){
        OrderItems orderItems = new OrderItems(1L,10,100F, false);
        when(orderItemsRepository.findById(orderItems.getId())).thenReturn(Optional.of(orderItems));
        when(orderItemsRepository.increaseAmount(orderItems.getId(),orderItems.getBookAmount())).thenReturn(1);

        orderItemsService.increaseAmount(orderItems.getId());
        verify(orderItemsRepository,times(1)).increaseAmount(orderItems.getId(),orderItems.getBookAmount());
    }



}
