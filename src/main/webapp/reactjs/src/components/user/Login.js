import React, { Component } from "react";
import { connect } from 'react-redux';
import { Card, Row, Alert, Form, Col, Button, InputGroup } from "react-bootstrap";
import { authenticateUser } from '../../services/index';


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = this.initialState;
    }

    initialState = {
        username: '', password: '',role: '', error: ''
    };

    validateUser = () => {
        this.props.authenticateUser(this.state.username, this.state.password);
        console.log('auth request');
        setTimeout(() => {
            if (this.state.username.trim().length === 0 || this.state.password.length === 0) {
                this.setState({ error: "Empty fields not permitted" });
            }

            if (this.props.auth.isLoggedIn) {
                sessionStorage.setItem("username", this.state.username)
                this.props.history.push("/");
            } else {
                debugger;
                this.setState({ error: "Invalid username or password" });
                return this.props.history.push("/login");
            }
        },500)
    };


    userChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        const { username, password, error } = this.state;
        return (
            <Row className="justify-content-md-center">
                {error && <Alert variant="danger">{error}</Alert>}
                <Card style={{ width: '35rem' }} className={"border border-dark bg-dark text-white text-center center-block"}>
                    <Card.Header>Login</Card.Header>
                    <Card.Body>
                        <Form.Group as={Col} controlId="formGridUsername">
                            <InputGroup>
                                <Form.Control required autoComplete="off" type="text" name="username"
                                    value={username} onChange={this.userChange}
                                    className={"bg-dark text-white"} placeholder="Enter username" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group as={Col} controlId="formGridPassword">
                            <InputGroup>
                                <Form.Control required autoComplete="off" type="password" name="password"
                                    value={password} onChange={this.userChange}
                                    className={"bg-dark text-white"} placeholder="Enter Password" />
                            </InputGroup>
                        </Form.Group>
                        <Card.Footer style={{ "textAlign": "right" }}>
                            <Button size="lg" type="button" variant="success" onClick={this.validateUser}>
                                Login
                            </Button>
                        </Card.Footer>
                    </Card.Body>
                </Card>
            </Row>
        );
    }
}
const mapStateToProps = state => {
    return {
        auth: state.auth
    }
};

const mapDispatchToProps = dispatch => {
    return {
        authenticateUser: (username, password) => dispatch(authenticateUser(username, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
