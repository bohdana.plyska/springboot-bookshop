import React, { Component } from "react";
import { Alert, Button, Card, Col, Form, InputGroup, Row } from "react-bootstrap";
import axios from "axios";
import * as AT from "../services/auth/authType";

class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = this.initialState;
    }

    initialState = {
        name: '', email: '', username: '', password: '', signupError: ''
    };

    signupUser = () => {
        const user = {
            name: this.state.name,
            email: this.state.email,
            username: this.state.username,
            password: this.state.password
        };
        if (this.state.password.length < 7) {
            this.setState(this.initialState);
            this.setState({ signupError: "password should contain at least 7 characters" });
        } else if (this.state.name.trim().length === 0 || this.state.email.trim().length === 0 || this.state.username.trim().length === 0) {
            this.setState({ signupError: "Empty fields not permitted" });
        } else {
            axios.post(AT.API_URL + "/signup", user)
                .then(response => {
                    if (response.data != null) {
                        this.setState(this.initialState);
                        alert("User " + response.data.username + " created successfully");
                        return this.props.history.push("/login");
                    }
                });
        }
    };


    signupChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <Row className="justify-content-md-center">
                {this.state.signupError && <Alert variant="danger">{this.state.signupError}</Alert>}
                <Card style={{ width: '35rem', textAlign: "center" }} className={"border border-dark bg-dark text-white"}>
                    <Card.Header>Signup</Card.Header>
                    <Card.Body>
                        <Form.Group as={Col}>
                            <InputGroup>
                                <Form.Control required autoComplete="off" type="text" name="name"
                                    value={this.state.name} onChange={this.signupChange}
                                    className={"bg-dark text-white"} placeholder="Enter Name" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Control required autoComplete="off" type="text" name="email"
                                value={this.state.email} onChange={this.signupChange}
                                className={"bg-dark text-white"} placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group as={Col}>
                            <Form.Control required autoComplete="off" type="text" name="username"
                                value={this.state.username} onChange={this.signupChange}
                                className={"bg-dark text-white"} placeholder="Enter username" />
                        </Form.Group>

                        <Form.Group as={Col}>
                            <Form.Control required autoComplete="off" type="password" name="password"
                                value={this.state.password} onChange={this.signupChange}
                                className={"bg-dark text-white"} placeholder="Enter Password" />
                        </Form.Group>

                        <Card.Footer style={{ "textAlign": "right" }}>
                            <Button size="lg" type="button" variant="success" onClick={this.signupUser}>
                                Signup
                            </Button>
                        </Card.Footer>
                    </Card.Body>
                </Card>
            </Row>
        );
    }
}

export default Signup