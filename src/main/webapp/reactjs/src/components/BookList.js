import React, { Component } from "react";
import { Button, ButtonGroup, Card, FormControl, InputGroup, Table } from "react-bootstrap";
import axios from "axios";
import { Link } from "react-router-dom";

export default class BookList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            books: [],
            search: ''
        };
        this.deleteBook = this.deleteBook.bind(this);
        this.searchData = this.searchData.bind(this)
    }

    componentDidMount() {
        this.findAllBooks();
    }

    findAllBooks() {
        axios.get("http://localhost:8080/books")
            .then(response => response.data)
            .then((data) => {
                this.setState({ books: data });
            });
    }

    deleteBook = (bookId) => {
        axios.delete("http://localhost:8080/books/delete/" + bookId)
            .then(response => {
                alert("Book Deleted successfully");
                this.setState({
                    books: this.state.books.filter(book => book.id !== bookId)
                });
            });
    };

    addBookToBasket = (bookId) => {
        const orderItem = {
            bookId,
        };

        axios.post("http://localhost:8080/addToBasket/" + bookId, orderItem)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("saved");
                    return this.props.history.push("/Basket");
                }
            });
    }

    searchData() {
        axios.get("http://localhost:8080/searchBook/" + this.state.search)
            .then(response => response.data)
            .then((data) => {
                this.setState({
                    books: data
                });
            });
    }

    searchChange = event => {
        this.setState({
            search: event.target.value
        });
    };

    cancelSearch = () => {
        this.setState({ "search": '' });
        this.findAllBooks();
    }

    buttonsLink = (book) => {
        const role = sessionStorage.getItem("role");
        let buttons;

        if(role === 'ROLE_ADMIN'){
            buttons = <><Link to={"edit/" + book.id} className="btn btn-sm btn-outline-primary"> Edit </Link> {' '}
                <Button size="sm" variant="outline-primary"
                        onClick={this.deleteBook.bind(this, book.id)}> Delete </Button>
                <Button size="sm" variant="outline-primary"
                        onClick={this.addBookToBasket.bind(this, book.id)}>Add To Basket
                </Button>
            </>
        }else{
            buttons = <>
                <Button size="sm" variant="outline-primary"
                        onClick={this.addBookToBasket.bind(this, book.id)}>Add To Basket
                </Button>
            </>
        }
        return buttons;
    }

    render() {

        return (
            <Card className="border border-dark bg-light text-dark">
                <Card.Header>
                    <div style={{ "float": "left" }}>
                        Book List
                    </div>
                    <div style={{ "float": "right" }}>
                        <InputGroup size="sm">
                            <FormControl placeholder="Search" name="search" value={this.state.search}
                                className="bg-dark text-white"
                                onChange={this.searchChange} />

                            <InputGroup.Append>
                                <Button size="sm" variant="outline-info" type="button" onClick={this.searchData}>
                                    Search
                                </Button>
                                <Button size="sm" variant="outline-danger" type="button" onClick={this.cancelSearch}>
                                    Clear
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                </Card.Header>
                <Card.Body>
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Publisher Name</th>
                                <th>Author Name</th>
                                <th>Title</th>
                                <th>Genre</th>
                                <th>NumberOfPages</th>
                                <th>Amount</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.books.length === 0 ?
                                <tr align="center">
                                    <td colSpan="6">Books Available.</td>
                                </tr> :
                                this.state.books.map((book) => (
                                    <tr key={book.id}>
                                        <td>{book.id}</td>
                                        <td>{book.name}</td>
                                        <td>{book.publisherName}</td>
                                        <td>{book.authorName}</td>
                                        <td>{book.title}</td>
                                        <td>{book.bookType}</td>
                                        <td>{book.numberOfPages}</td>
                                        <td>{book.amount}</td>
                                        <td>{book.price}</td>
                                        <td>
                                            <ButtonGroup>
                                                {this.buttonsLink(book)}
                                            </ButtonGroup>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        );
    }
}