import React, { Component } from "react";
import { Card, Form, Button, Col } from "react-bootstrap";
import axios from "axios";
import MyToast from "./MyToast";

export default class Book extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookTypes: [],
        };
        this.state.show = false;
        this.bookChange = this.bookChange.bind(this);
        this.submitBook = this.submitBook.bind(this);
    }

    initialState = {
        id: '', name: '', authorName: '', publisherName: '', bookType: '', price: '', title: '', numberOfPages: '', amount: ''
    };

    componentDidMount() {
        const bookId = +this.props.match.params.id;
        if (bookId) {
            this.findBookById(bookId);
        }
        this.findAllBookTypes();
    }

    findAllBookTypes = () => {
        axios.get("http://localhost:8080/genres")
            .then(response => response.data)
            .then((data) => {
                this.setState({
                    bookTypes: [{ value: '', disaplay: 'Select BookType' }]
                        .concat(data.map(bookType => {
                            return { value: bookType, display: bookType }
                        }))
                });
            });
    };

    findBookById = (bookId) => {
        axios.get("http://localhost:8080/books/" + bookId)
            .then(response => {
                if (response.data != null) {
                    this.setState({
                        id: response.data.id,
                        name: response.data.name,
                        authorName: response.data.authorName,
                        publisherName: response.data.publisherName,
                        bookType: response.data.bookType,
                        price: response.data.price,
                        title: response.data.title,
                        numberOfPages: response.data.numberOfPages,
                        amount: response.data.amount
                    });
                }
            }).catch((error) => {
                console.error("Error - " + error);
            });
    }



    submitBook = event => {
        event.preventDefault();

        const book = {
            name: this.state.name,
            authorName: this.state.authorName,
            publisherName: this.state.publisherName,
            bookType: this.state.bookType,
            price: +this.state.price,
            title: this.state.title,
            numberOfPages: this.state.numberOfPages,
            amount: this.state.amount
        };

        console.log(book);

        axios.post("http://localhost:8080/saveBook", book)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("saved");
                    return this.props.history.push("/listBooks");
                }
            });
    }

    updateBook = event => {
        event.preventDefault();

        const book = {
            id: this.state.id,
            name: this.state.name,
            authorName: this.state.authorName,
            publisherName: this.state.publisherName,
            bookType: this.state.bookType,
            price: +this.state.price,
            title: this.state.title,
            numberOfPages: this.state.numberOfPages,
            amount: this.state.amount
        };
        axios.post("http://localhost:8080/saveBook", book)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("update");
                    return this.props.history.push("/listBooks");
                }
            });
    }

    resetBook = () => {
        this.setState(() => this.initialState);
    }

    bookChange(event) {
        console.log(event.target.name, event.target.value);
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    bookList = () => {
        return this.props.history.push("/listBooks");
    };

    render() {
        const { name, authorName, publisherName, bookType, price, title, numberOfPages, amount } = this.state;
        return (
            <div>
                <div style={{ "display": this.state.show ? "block" : "none" }}>
                    <MyToast children={{ show: this.state.show }} />
                </div>
                <Card className="border border-dark bg-light text-dark">
                    <Card.Header> {this.state.id ? "Update Book" : "Save Book "}</Card.Header>
                    <Form onReset={this.resetBook} onSubmit={this.state.id ? this.updateBook : this.submitBook} id="bookFormId">
                        <Card.Body>
                            <Form.Group as={Col} controlId="formGridName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" name="name" autoComplete="off"
                                    value={name} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Name" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridAuthorName">
                                <Form.Label>AuthorName</Form.Label>
                                <Form.Control type="text" name="authorName" autoComplete="off"
                                    value={authorName} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Book AuthorName" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridPublisherName">
                                <Form.Label>Publisher Name</Form.Label>
                                <Form.Control type="text" name="publisherName" autoComplete="off"
                                    value={publisherName} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Publisher Name" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridGenre">
                                <Form.Label>BookType</Form.Label>
                                <Form.Control required as="select"
                                    custom onChange={this.bookChange}
                                    name="bookType" value={bookType}
                                    className={"bg-dark text-white "}>
                                    {this.state.bookTypes.map(bookType =>
                                        <option key={bookType.value} value={bookType.value}>
                                            {bookType.display}
                                        </option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridPrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control type="text" name="price" autoComplete="off"
                                    value={price} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Price" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridTitle">
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" name="title" autoComplete="off"
                                    value={title} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Book Title" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridTitle">
                                <Form.Label>Amount</Form.Label>
                                <Form.Control type="text" name="amount" autoComplete="off"
                                    value={amount} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Amount" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridTitle">
                                <Form.Label>NumberOfPages</Form.Label>
                                <Form.Control type="text" name="numberOfPages" autoComplete="off"
                                    value={numberOfPages} onChange={this.bookChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter NumberOfPages" />
                            </Form.Group>
                        </Card.Body>
                        <Card.Footer style={{ "textAlight": "right", "marginBottom": "100px" }}>
                            <Button size="sm" variant="success" type="submit">
                                {this.state.id ? "Update" : "Save"}
                            </Button> {' '}
                            <Button size="sm" variant="info" type="reset">
                                Reset
                            </Button> {' '}
                            <Button size="sm" variant="info" type="button" onClick={this.bookList.bind()}>
                                Book List
                            </Button> {' '}
                        </Card.Footer>
                    </Form>
                </Card >
            </div>
        );
    }
}
