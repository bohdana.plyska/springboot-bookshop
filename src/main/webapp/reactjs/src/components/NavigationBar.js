import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { logoutUser } from "../services";


class NavigationBar extends React.Component {

    constructor(props) {
        super(props);
    }

    logout = () => {
        this.props.logoutUser();
    };

    getLink() {
        const guestLinks = (
            <>
                <Nav className="navbar-right">
                    <Link to={"signup"} className="nav-link"> Signup</Link>
                    <Link to={"login"} className="nav-link"> Login</Link>
                </Nav>
            </>
        );

        const userLinks = (
            <>
                <Nav className="me-auto">
                    <Link to={"listBooks"} className="nav-link">Book List</Link>
                    <Link to={"Basket"} className="nav-link">Basket</Link>
                    <Link to={"listOrders"} className="nav-link">Order List</Link>
                </Nav>
                <Nav className="navbar-right">
                    <Link to={"logout"} className="nav-link" onClick={this.logout}> Logout</Link>
                </Nav>
            </>
        );

        const adminLinks = (
            <>
                <Nav className="me-auto">
                    <Link to={"addBook"} className="nav-link">Add Book</Link>
                    <Link to={"addUser"} className="nav-link">Add User</Link>
                    <Link to={"listBooks"} className="nav-link">Book List</Link>
                    <Link to={"listUsers"} className="nav-link">User List</Link>
                    <Link to={"Basket"} className="nav-link">Basket</Link>
                    <Link to={"listOrders"} className="nav-link">Order List</Link>
                </Nav>
                <Nav className="navbar-right">
                    <Link to={"logout"} className="nav-link" onClick={this.logout}> Logout</Link>
                </Nav>
            </>
        );

        console.log('Load nav bar')
        const authenteficated = localStorage.getItem("jwtToken");

            const auth = authenteficated ? true : false;
            if (auth) {
                let role = localStorage.getItem("role")
                console.error(role);
                if (role === 'ROLE_ADMIN') {
                    return adminLinks;
                } else {
                    return userLinks;
                }
            } else {
                return guestLinks;
            }

    }
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Link to={"/"} className="navbar-brand">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/b/ba/Book_icon_1.png" width="25" height="25" alt="brand" />Book Shop
                </Link>
                {this.getLink()}
            </Navbar>
        );

    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);
