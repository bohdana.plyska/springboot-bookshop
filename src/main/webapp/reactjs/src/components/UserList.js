import React, { Component } from 'react';
import axios from "axios";
import { ButtonGroup, Card, Table, Button, InputGroup, FormControl } from "react-bootstrap";
import { Link } from "react-router-dom";


export default class UserList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            search: ''
        };
        this.deleteUser = this.deleteUser.bind(this);
        this.searchData = this.searchData.bind(this)
    }

    componentDidMount() {
        this.findAllUsers();
    }

    findAllUsers() {
        axios.get("http://localhost:8080/users")
            .then(response => response.data)
            .then((data) => {
                this.setState({ users: data });
            });
    }

    deleteUser = (userId) => {
        axios.delete("http://localhost:8080/users/delete/" + userId)
            .then(response => {
                alert("User Deleted successfully");
                this.setState({
                    users: this.state.users.filter(user => user.id !== userId)
                });
            });
    };

    searchData() {
        axios.get("http://localhost:8080/searchUser/" + this.state.search)
            .then(response => response.data)
            .then((data) => {
                this.setState({
                    users: data
                });
            });
    }

    searchChange = event => {
        this.setState({
            search: event.target.value
        });
    };

    cancelSearch = () => {
        this.setState({ "search": '' });
        this.findAllUsers();
    }


    render() {
        return (
            <Card className="border border-dark bg-light text-dark">
                <Card.Header>
                    <div style={{ "float": "left" }}>
                        User List
                    </div>
                    <div style={{ "float": "right" }}>
                        <InputGroup size="sm">
                            <FormControl placeholder="Search" name="search" value={this.state.search}
                                className="bg-dark text-white"
                                onChange={this.searchChange} />
                            <InputGroup.Append>
                                <Button size="sm" variant="outline-info" type="button" onClick={this.searchData}>
                                    Search
                                </Button>
                                <Button size="sm" variant="outline-danger" type="button" onClick={this.cancelSearch}>
                                    Clear
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                </Card.Header>
                <Card.Body>
                    <Table striped bordered hover variant="dark" className="center">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.users.length === 0 ?
                                <tr align="center">
                                    <td colSpan="6"> No Users Available.</td>
                                </tr> :
                                this.state.users.map((user) => (
                                    <tr key={user.id}>
                                        <td>{user.username}</td>
                                        <td>{user.email}</td>
                                        <td>
                                            <ButtonGroup>
                                                <Link to={"editUser/" + user.id} className="btn btn-sm btn-outline-primary"> Edit </Link> {' '}
                                                <Button size="sm" variant="outline-primary"
                                                    onClick={this.deleteUser.bind(this, user.id)}> Delete</Button>
                                            </ButtonGroup>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        );
    }
}