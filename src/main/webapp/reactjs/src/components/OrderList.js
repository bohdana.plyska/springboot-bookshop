import React, { Component } from 'react';
import axios from "axios";
import { Card, Table } from "react-bootstrap";

class OrderList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        }
    }
    componentDidMount() {
        this.findAllOrders();
    }

    findAllOrders() {
        axios.get("http://localhost:8080/showOrders")
            .then(response => response.data)
            .then((data) => {
                this.setState({ orders: data });
            });
    }

    render() {
        return (
            <Card className="border border-dark bg-light text-dark">
                <Card.Header>
                    Orders
                </Card.Header>
                <Card.Body>
                    <Table striped bordered hover variant="dark" className="center">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.orders.length === 0 ?
                                <tr align="center">
                                    <td colSpan="6"> No Orders Available.</td>
                                </tr> :
                                this.state.orders.map((order) => (
                                    <tr key={order.id}>
                                        <td>{order.id}</td>
                                        <td>{order.price}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        );
    }
}

export default OrderList;