import React, { Component } from 'react';
import axios from "axios";
import { Button, Card, Col, Form } from "react-bootstrap";

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.state.show = false;
        this.userChange = this.userChange.bind(this);
    }


    initialState = {
        id: '', username: '', password: '', passwordConfirm: '',
        email: '', city: '', country: '', district: '',
        streetAddress: '', zipcode: '', birthDate: ''
    };

    findUserById = (userId) => {
        axios.get("http://localhost:8080/users/" + userId)
            .then(response => {
                if (response.data != null) {
                    this.setState({
                        id: response.data.id,
                        username: response.data.username,
                        password: response.data.password,
                        passwordConfirm: response.data.passwordConfirm,
                        email: response.data.email,
                        city: response.data.city,
                        country: response.data.country,
                        district: response.data.district,
                        streetAddress: response.data.streetAddress,
                        zipcode: response.data.zipcode,
                        birthDate: response.data.birthDate
                    });
                }
            }).catch((error) => {
                console.error("Error - " + error);
            });
    }

    submitUser = event => {
        event.preventDefault();

        const user = {
            username: this.state.username,
            password: this.state.password,
            passwordConfirm: this.state.passwordConfirm,
            email: this.state.email,
            city: +this.state.city,
            country: this.state.country,
            district: this.state.district,
            streetAddress: this.state.streetAddress,
            zipcode: this.state.zipcode,
            birthDate: new Date(this.state.birthDate).toISOString(),
        };

        axios.post("http://localhost:8080/save", user)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("saved");
                    return this.props.history.push("/listUsers");
                }
            });
    }

    updateUser = event => {
        event.preventDefault();

        const user = {
            username: this.state.username,
            password: this.state.password,
            passwordConfirm: this.state.passwordConfirm,
            email: this.state.email,
            city: this.state.city,
            country: this.state.country,
            district: this.state.district,
            streetAddress: this.state.streetAddress,
            zipcode: this.state.zipcode,
            birthDate: this.state.birthDate
        };
        axios.post("http://localhost:8080/save", user)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("update");
                    return this.props.history.push("/listUsers");
                }
            });
    }

    resetUser = () => {
        this.setState(() => this.initialState);
    }

    userChange(event) {
        console.log(event.target.name, event.target.value)
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    userList = () => {
        return this.props.history.push("/listUsers");
    };

    render() {
        const { username, password, passwordConfirm, email, city, country, district, streetAddress, zipcode, birthDate } = this.state;
        return (
            <div>
                <Card className="border border-dark bg-light text-dark">
                    <Card.Header> {this.state.id ? "Update User" : "Save User "}</Card.Header>
                    <Form onReset={this.resetUser} onSubmit={this.state.id ? this.updateUser : this.submitUser} id="userFormId">
                        <Card.Body>
                            <Form.Group as={Col} controlId="formGridUserName">
                                <Form.Label>UserName</Form.Label>
                                <Form.Control type="text" name="username" autoComplete="off"
                                    value={username} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter UserName" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="password" autoComplete="off"
                                    value={password} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Password" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridPasswordConfirm">
                                <Form.Label>PasswordConfirm</Form.Label>
                                <Form.Control type="password" name="passwordConfirm" autoComplete="off"
                                    value={passwordConfirm} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter PasswordConfirm" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="text" name="email" autoComplete="off"
                                    value={email} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Email" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridCity">
                                <Form.Label>City</Form.Label>
                                <Form.Control type="text" name="city" autoComplete="off"
                                    value={city} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter City" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridCountry">
                                <Form.Label>Country</Form.Label>
                                <Form.Control type="text" name="country" autoComplete="off"
                                    value={country} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Country" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridDistrict">
                                <Form.Label>District</Form.Label>
                                <Form.Control type="text" name="district" autoComplete="off"
                                    value={district} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter District" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridStreetAddress">
                                <Form.Label>StreetAddress</Form.Label>
                                <Form.Control type="text" name="streetAddress" autoComplete="off"
                                    value={streetAddress} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter StreetAddress" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridZipcode">
                                <Form.Label>Zipcode</Form.Label>
                                <Form.Control type="text" name="zipcode" autoComplete="off"
                                    value={zipcode} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter Zipcode" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridBirthDate">
                                <Form.Label>BirthDate</Form.Label>
                                <Form.Control type="text" name="birthDate" autoComplete="off"
                                    value={birthDate} onChange={this.userChange}
                                    className={"bg-dark text-white"}
                                    placeholder="Enter BirthDate" />
                            </Form.Group>
                        </Card.Body>
                        <Card.Footer style={{ "textAlight": "right", "marginBottom": "100px" }}>
                            <Button size="sm" variant="success" type="submit">
                                {this.state.id ? "Update" : "Save"}
                            </Button> {' '}
                            <Button size="sm" variant="info" type="reset">
                                Reset
                            </Button> {' '}
                            <Button size="sm" variant="info" type="button" onClick={this.userList.bind()}>
                                User List
                            </Button> {' '}
                        </Card.Footer>
                    </Form>
                </Card >
            </div>
        );
    }
}

export default User;