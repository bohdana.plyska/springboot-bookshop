import React, { Component } from 'react';
import axios from "axios";
import { Button, ButtonGroup, Card, Table } from "react-bootstrap";

class OrderItemList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderItems: [],
        };
        this.deleteOrderItem = this.deleteOrderItem.bind(this);
    }

    initialState = {
        id: '', bookAmount: '', pinned: '', price: ''
    };

    componentDidMount() {
        this.findAllOrderItems();
    }

    findAllOrderItems() {
        axios.get("http://localhost:8080/showBasket")
            .then(response => response.data)
            .then((data) => {
                this.setState({ orderItems: data });
            });
    }

    deleteOrderItem = (orderItemId) => {
        axios.delete("http://localhost:8080/deleteOrderItem/" + orderItemId)
            .then(response => {
                alert("User Deleted successfully");
                this.setState({
                    orderItems: this.state.orderItems.filter(orderItem => orderItem.id !== orderItemId)
                });
            });
    };

    increaseAmount = (orderItemId) => {

        axios.post("http://localhost:8080/increaseAmount/" + orderItemId)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("increase +1");
                    this.findAllOrderItems();
                    return this.props.history.push("/Basket");
                }
            });
    }

    decreaseAmount = (orderItemId) => {
        axios.post("http://localhost:8080/decreaseAmount/" + orderItemId)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("decrease -1");
                    this.findAllOrderItems();
                    return this.props.history.push("/Basket");
                }
            });
    }

    createOrder() {
        axios.post("http://localhost:8080/createOrder")
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    alert("Order Done!");
                    return this.props.history.push("/listOrders");
                }
            });

    }

    render() {
        return (
            <Card className="border border-dark bg-light text-dark">
                <Card.Header>
                    Basket
                </Card.Header>
                <Card.Body>
                    <Table striped bordered hover variant="dark" className="center">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>BookAmount</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.orderItems.length === 0 ?
                                <tr align="center">
                                    <td colSpan="6"> No Basket Item Available.</td>
                                </tr> :
                                this.state.orderItems.map((orderItem) => (
                                    <tr key={orderItem.id}>
                                        <td>{orderItem.id}</td>
                                        <td>{orderItem.bookAmount}</td>
                                        <td>{orderItem.bookAmount * orderItem.price}</td>
                                        <td>
                                            <ButtonGroup>
                                                <Button size="sm" variant="outline-primary"
                                                    onClick={this.increaseAmount.bind(this, orderItem.id)}>IncreaseAmount</Button>
                                                <Button size="sm" variant="outline-primary"
                                                    onClick={this.decreaseAmount.bind(this, orderItem.id)}>DecreaseAmount</Button>
                                                <Button size="sm" variant="outline-primary"
                                                    onClick={this.deleteOrderItem.bind(this, orderItem.id)}> Delete</Button>
                                            </ButtonGroup>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Card.Body>
                <Card.Footer style={{ "textAlight": "right" }}>
                    <Button size="sm" variant="success"
                        onClick={this.createOrder.bind(this)}>Order
                    </Button>
                </Card.Footer>
            </Card>
        );
    }
}

export default OrderItemList;