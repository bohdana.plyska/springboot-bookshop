import React from "react";

class Welcome extends React.Component {
    render() {
        return (
            <div className="container mt-3">
                <div className="mt-4 p-5 bg-dark text-white rounded text-center">
                    <h1>Welcome to Book Shop</h1>
                </div>
            </div>
        );
    }

}
export default Welcome;