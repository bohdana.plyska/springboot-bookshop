import React from 'react';
import './App.css';
import NavigationBar from './components/NavigationBar.js';
import Welcome from "./components/Welcome";
import Footer from "./components/Footer";
import BookList from "./components/BookList";
import Book from "./components/Book";
import Login from "./components/user/Login";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Signup from "./components/Signup";
import UserList from "./components/UserList";
import { Container, Row, Col } from "react-bootstrap";
import User from "./components/User";
import OrderItemList from "./components/OrderItemList";
import OrderList from "./components/OrderList";
import NotFound from "./components/NotFound";


//  function getRouts(){
//    const adminLinks =(
//      <>
//        <Route path="/" exact component={Welcome} />
//        <Route path="/addBook" exact component={Book} />
//        <Route path="/edit/:id" exact component={Book} />
//        <Route path="/addUser" exact component={User} />
//        <Route path="/editUser/:id" exact component={User} />
//        <Route path="/listBooks" exact component={BookList} />
//        <Route path="/listUsers" exact component={UserList} />
//        <Route path="/Basket" exact component={OrderItemList} />
//        <Route path="/listOrders" exact component={OrderList} />
//        <Route path="/signup" exact component={Signup} />
//        <Route path="/login" exact component={Login} />
//        <Route path="/logout" exact component={Login} />
//        <Route path="*" component={NotFound} />
//      </>
//    )
//
//    const userLinks =(
//        <>
//          <Route path="/" exact component={Welcome} />
//          <Route path="/listBooks" exact component={BookList} />
//          <Route path="/Basket" exact component={OrderItemList} />
//          <Route path="/listOrders" exact component={OrderList} />
//          <Route path="/signup" exact component={Signup} />
//          <Route path="/login" exact component={Login} />
//          <Route path="/logout" exact component={Login} />
//          <Route path="*" component={NotFound} />
//        </>
//    )
//
//    const guestLinks =(
//        <>
//          <Route path="/" exact component={Welcome} />
//          <Route path="/signup" exact component={Signup} />
//          <Route path="/listBooks" exact component={BookList} />
//          <Route path="/login" exact component={Login} />
//          <Route path="/logout" exact component={Login} />
//          <Route path="*" component={NotFound} />
//        </>
//    )
//    const authenteficated = localStorage.getItem("jwtToken");
//    console.log(authenteficated)
//    const auth = authenteficated ? true : false;
//    console.log(auth);
//       if(auth){
//         const role = sessionStorage.getItem("role")
//         console.log(role);
//         if( role === 'ROLE_ADMIN'){
//           return adminLinks;
//         } else {
//           return userLinks;
//         }
//       } else {
//         return guestLinks;
//       }
// }

function App() {

  const marginTop = {
    marginTop: "20px"
  };

  return (
    <Router>
      <NavigationBar />
      <Container>
        <Row>
          <Col lg={12} style={marginTop}>
            <Switch>
              <Route path="/" exact component={Welcome} />
              <Route path="/addBook" exact component={Book} />
              <Route path="/edit/:id" exact component={Book} />
              <Route path="/addUser" exact component={User} />
              <Route path="/editUser/:id" exact component={User} />
              <Route path="/listBooks" exact component={BookList} />
              <Route path="/listUsers" exact component={UserList} />
              <Route path="/Basket" exact component={OrderItemList} />
              <Route path="/listOrders" exact component={OrderList} />
              <Route path="/signup" exact component={Signup} />
              <Route path="/login" exact component={Login} />
              <Route path="/logout" exact component={Login} />
              <Route path="*" component={NotFound} />
            </Switch>
          </Col>
        </Row>
      </Container>
      <Footer />
    </Router>
  );
}

export default App;
