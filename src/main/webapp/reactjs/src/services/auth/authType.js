export const API_URL = 'http://localhost:8080'
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';