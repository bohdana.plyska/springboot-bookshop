import axios from 'axios';
import * as AT from './authType';



export const authenticateUser = (username, password) => {
    const credentials = {
        username: username,
        password: password
    };

    function findUserByUserName(userName)  {
        return axios.get("http://localhost:8080/users/findByUserName/" + userName)
            .then(res => {
                console.log(res.data);
                localStorage.setItem('role', res.data.role);
            })
    }

    return dispatch => {
        dispatch({
            type: AT.LOGIN_REQUEST
        });
        axios.post(AT.API_URL+"/login", credentials)
            .then(async (response) => {
                console.log(response)
                let token = response.headers.authorization;
                console.warn("token", token);
                localStorage.setItem('jwtToken', token);
                sessionStorage.setItem("username", username)
                findUserByUserName(username).then((res) => {
                    dispatch(success(true));
                })
            })
            .catch(error => {
                dispatch(failure());
            });
    };
};

export const logoutUser = () => {
    return dispatch => {
        dispatch({
            type: AT.LOGOUT_REQUEST
        });
        localStorage.removeItem('jwtToken');
        sessionStorage.removeItem('username');
        localStorage.removeItem('role');
        dispatch(success(false));
    };
};

const success = isLoggedIn => {
    return {
        type: AT.SUCCESS,
        payload: isLoggedIn
    };
};

const failure = () => {
    return {
        type: AT.FAILURE,
        payload: false
}
};