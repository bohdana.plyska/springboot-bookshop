package com.bohdana.bookshop.repository;

import com.bohdana.bookshop.entity.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    @Modifying
    @Transactional
    @Query("SELECT o FROM Order o WHERE o.user.id = ?1")
    List<Order> getOrdersByUserId(Long userId);

}
