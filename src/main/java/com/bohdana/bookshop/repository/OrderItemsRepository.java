package com.bohdana.bookshop.repository;

import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.OrderItems;
import com.bohdana.bookshop.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface OrderItemsRepository extends CrudRepository<OrderItems,Long> {

    @Modifying
    @Transactional
    @Query(value = "select o from OrderItems o WHERE o.pinned = false and o.userId.id = ?1")
    List<OrderItems> findUnpinnedUserOrderDetails(Long userId);

    @Query("SELECT o FROM OrderItems o where o.bookId = ?1 and  o.userId = ?2 and o.pinned = false")
    OrderItems findByKeyParam(Book bookId, User useId);

    @Modifying
    @Transactional
    @Query("UPDATE OrderItems o set o.bookAmount = ?2 + 1 WHERE o.id = ?1")
    int increaseAmount( Long id, Integer bookAmount);

    @Modifying
    @Transactional
    @Query("UPDATE OrderItems o set o.bookAmount = ?2 - 1  WHERE o.id = ?1")
    int decreaseAmount( Long id, Integer bookAmount);


    @Modifying
    @Transactional
    @Query("SELECT o FROM OrderItems o WHERE o.userId.id= ?1 AND o.pinned = false")
    List<OrderItems> getOrderItemsForBasket(Long userId);
}
