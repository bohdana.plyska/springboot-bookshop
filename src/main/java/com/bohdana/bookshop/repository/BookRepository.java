package com.bohdana.bookshop.repository;

import com.bohdana.bookshop.entity.Book;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

    Optional<Book> findByName(String name);
    Optional<Book> getById(Long id);

    @Modifying
    @Transactional
    @Query(value = " select b from Book b where b.name like %:keyword% or b.title like %:keyword%")
    ArrayList<Book> search(String keyword);

    @Query(value = "select b from Book b where b.amount > 0")
    List<Book> findAllAvailable();
}
