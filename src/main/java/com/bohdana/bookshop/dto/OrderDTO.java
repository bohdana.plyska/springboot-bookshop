package com.bohdana.bookshop.dto;

import lombok.Data;

@Data
public class OrderDTO {
    private Long id;
    private double price;
    private boolean handled;
}
