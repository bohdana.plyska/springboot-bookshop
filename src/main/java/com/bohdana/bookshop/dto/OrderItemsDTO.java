package com.bohdana.bookshop.dto;

import lombok.Data;
@Data
public class OrderItemsDTO {
    private Long bookId;
    private Long userId;

    public OrderItemsDTO(Long bookId, Long userId) {
        this.bookId = bookId;
        this.userId = userId;
    }
}
