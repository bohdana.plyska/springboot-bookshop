package com.bohdana.bookshop.dto;

import lombok.Data;

@Data
public class BookDTO {
    private Long id;
    private String name;
    private String publisherName;
    private String authorName;
    private String title;
    private Float price;
    private Long numberOfPages;
    private Long amount;
    private String bookType;
}
