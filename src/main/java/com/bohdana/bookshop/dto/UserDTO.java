package com.bohdana.bookshop.dto;

import com.bohdana.bookshop.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class UserDTO {
    private Long id;
    private String username;
    private String password;
    private String passwordConfirm;
    private String email;
    private String city;
    private String country;
    private String district;
    private String streetAddress;
    private String zipcode;
    private LocalDate birthDate;
    private String salt;
    private Role role;

    public UserDTO () {

    }

    public UserDTO(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
