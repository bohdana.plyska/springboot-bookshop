package com.bohdana.bookshop.mappers;

import com.bohdana.bookshop.dto.OrderDTO;
import com.bohdana.bookshop.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderDTO orderToOrderDto(Order order);
}
