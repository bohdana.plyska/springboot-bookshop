package com.bohdana.bookshop.mappers;

import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.GenreEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BookMapper {
    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    BookDTO bookToBookDto(Book book);

    @Mapping(source = "bookDTO.bookType", target = "bookType")
    Book bookDtoToBook(BookDTO bookDTO);

    default GenreEnum map(String s) {
        return GenreEnum.getValueByString(s);

    }
}
