package com.bohdana.bookshop.service;

import com.bohdana.bookshop.dto.OrderItemsDTO;
import com.bohdana.bookshop.entity.OrderItems;
import java.util.List;

public interface OrderItemsService {

    OrderItems addOrderItems(OrderItemsDTO orderItemsDTO);

    void delete(Long orderItemsId);

    int increaseAmount(Long orderItemId);

    int decreaseAmount(Long orderItemId);

    List<OrderItems> getOrderItemsForBasket(Long userId);
}
