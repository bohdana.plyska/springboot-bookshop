package com.bohdana.bookshop.service;

import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.entity.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface BookService {

    Book save(Book bookDTO);

    void delete(Long id);

    Book update(BookDTO bookDTO);

    BookDTO findById(Long id);

    BookDTO findByName(String name);

    List<Book> listAllBooks();

    ArrayList<BookDTO> search(String keyword);

    List<Book> findAllAvailable();

    boolean updateBookAmount(Long bookId, Integer bookAmount);

    Optional<Book> getBook(Long id);
}
