package com.bohdana.bookshop.service;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface UserService {

    User save(UserDTO userDTO);

    void delete(Long userId);

    User update(UserDTO userDTO);

    UserDTO findById(Long userId);

    UserDTO findByUsername(String username);

    UserDTO findByEmail(String email);

    List<User> listAllUsers();

    ArrayList<UserDTO> search(String keyword);

    User getUserByName(String username);

    Optional<User> getUser(Long id);
}
