package com.bohdana.bookshop.service;

public interface HashService {
    String getHashedValue(String data, String salt);
}
