package com.bohdana.bookshop.service;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);

}
