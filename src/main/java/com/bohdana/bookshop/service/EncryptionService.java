package com.bohdana.bookshop.service;

public interface EncryptionService {

    String encryptValue(String data, String key);

    String decryptValue(String data, String key);
}
