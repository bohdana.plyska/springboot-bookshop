package com.bohdana.bookshop.service.impl;

import com.bohdana.bookshop.dto.OrderItemsDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.OrderItems;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.exception.BookSearchException;
import com.bohdana.bookshop.exception.OrderItemsIsPinnedException;
import com.bohdana.bookshop.exception.UserSearchException;
import com.bohdana.bookshop.repository.BookRepository;
import com.bohdana.bookshop.repository.OrderItemsRepository;
import com.bohdana.bookshop.repository.UserRepository;
import com.bohdana.bookshop.service.OrderItemsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderItemsServiceImpl implements OrderItemsService {

    private final OrderItemsRepository orderItemsRepository;

    private final BookRepository bookRepository;

    private final UserRepository userRepository;

    @Override
    public OrderItems addOrderItems(OrderItemsDTO orderItemsDTO) {
        Optional<Book> bookSearch = bookRepository.findById(orderItemsDTO.getBookId());

        if (bookSearch.isEmpty()) {
            throw new BookSearchException("Didn't find book");
        }

        Optional<User> userSearch = userRepository.findById(orderItemsDTO.getUserId());

        if (userSearch.isEmpty()) {
            throw new UserSearchException("Didn't find user");
        }

        OrderItems processedOrderItems;
        Optional<OrderItems> existOrderItem = Optional.ofNullable(
                orderItemsRepository.findByKeyParam(
                        bookSearch.get(),
                        userSearch.get()
                )
        );

        if (existOrderItem.isPresent()) {
            processedOrderItems = existOrderItem.get();
            processedOrderItems.setBookAmount(processedOrderItems.getBookAmount() + 1);
        } else {
            processedOrderItems = new OrderItems(
                    bookSearch.get(),
                    1,
                    userSearch.get(),
                    bookSearch.get().getPrice()
            );
        }

        return orderItemsRepository.save(processedOrderItems);
    }

    @Override
    public void delete(Long orderItemsId) {
        if (orderItemsRepository.findById(orderItemsId).get().isPinned()) {
            throw new OrderItemsIsPinnedException("Order Details are pinned");
        } else {
            orderItemsRepository.deleteById(orderItemsId);
        }
    }

    @Override
    public int increaseAmount(Long orderItemId) {
        Optional<OrderItems> orderItem = orderItemsRepository.findById(orderItemId);
        return orderItemsRepository.increaseAmount(orderItemId, orderItem.get().getBookAmount());
    }

    @Override
    public int decreaseAmount(Long orderItemId) {
        Optional<OrderItems> orderItem = orderItemsRepository.findById(orderItemId);
        return orderItemsRepository.decreaseAmount(orderItemId, orderItem.get().getBookAmount());
    }

    @Override
    public List<OrderItems> getOrderItemsForBasket(Long userId){
            return orderItemsRepository.getOrderItemsForBasket(userId);
    }
}
