package com.bohdana.bookshop.service.impl;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.Role;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.exception.UserNotFoundException;
import com.bohdana.bookshop.mappers.UserMapper;
import com.bohdana.bookshop.repository.UserRepository;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostConstruct
    public void postConstruct() {
        Optional<User> user = userRepository.findById(DEFAULT_USER_ADMIN_ID);
        if (user.isEmpty()) {
            User admin = new User();
            admin.setId(DEFAULT_USER_ADMIN_ID);
            admin.setUsername(DEFAULT_USER_NAME_ADMIN);
            admin.setRole(Role.ROLE_ADMIN);
            admin.setPassword(bCryptPasswordEncoder.encode(DEFAULT_USER_PASSWORD_FOR_ADMIN));
            admin.setEmail(DEFAULT_USER_ADMIN_EMAIL);
            userRepository.save(admin);
        }
    }

    @Override
    public User save(UserDTO userDTO) {
        if (userDTO.getPassword() != null){
            userDTO.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        }
        userDTO.setRole(Role.ROLE_USER);
        return userRepository.save(UserMapper.INSTANCE.userDtoToUser(userDTO));
    }

    @Override
    public void delete(Long userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        userOptional.ifPresent(userRepository::delete);
    }

    public List<User> listAllUsers() {
        return  (List<User>) userRepository.findAll();
    }

    @Override
    public User update(UserDTO userDTO) {
        return userRepository.save(UserMapper.INSTANCE.userDtoToUser(userDTO));
    }

    public ArrayList<UserDTO> search(String keyboard){
        ArrayList<User> users = userRepository.searchUsersByName(keyboard);
        ArrayList<UserDTO> result = new ArrayList<>();

        for (User item : users) {
            result.add(UserMapper.INSTANCE.userToUserDto(item));
        }
        return result;
    }

    @Override
    public UserDTO findById(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()){
            return UserMapper.INSTANCE.userToUserDto(optionalUser.get());
        }
        throw new UserNotFoundException("User with such id doesn't exist");
    }

    @Override
    public UserDTO findByUsername(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        return optionalUser.map(UserMapper.INSTANCE::userToUserDto).orElse(null);
    }

    @Override
    public UserDTO findByEmail(String email) {
        Optional<User> optionalUser = userRepository.findByEmail(email);
        return optionalUser.map(UserMapper.INSTANCE::userToUserDto).orElse(null);
    }

    @Override
    public User getUserByName(String username){
        return userRepository.getUserByUsername(username);
    }

    public Optional<User> getUser(Long id) {
        return userRepository.findById(id);
    }

}
