package com.bohdana.bookshop.service.impl;

import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.exception.BookNotFoundException;
import com.bohdana.bookshop.mappers.BookMapper;
import com.bohdana.bookshop.repository.BookRepository;
import com.bohdana.bookshop.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public Book save(Book bookDTO) {
        return bookRepository.save(bookDTO);
    }

    @Override
    public Book update(BookDTO bookDTO) {
        return bookRepository.save(BookMapper.INSTANCE.bookDtoToBook(bookDTO));
    }

    @Override
    public boolean updateBookAmount(Long bookId, Integer bookAmount) {
        Optional<Book> book = bookRepository.findById(bookId);
        book.ifPresent(value ->value.setAmount(value.getAmount() - bookAmount));
        return true;
    }

    @Override
    public void delete(Long id) {
        Optional<Book> bookOptional = bookRepository.findById(id);
        bookOptional.ifPresent(bookRepository::delete);
    }

    @Override
    public BookDTO findById(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            return BookMapper.INSTANCE.bookToBookDto(optionalBook.get());
        }
        throw new BookNotFoundException("Boor with such id doesn't exist");
    }

    @Override
    public BookDTO findByName(String name) {
        Optional<Book> optionalBook = bookRepository.findByName(name);
        return optionalBook.map(BookMapper.INSTANCE::bookToBookDto).orElse(null);
    }

    @Override
    public List<Book> listAllBooks() {
        return (List<Book>) bookRepository.findAll();
    }

    @Override
    public ArrayList<BookDTO> search(String keyword) {
        ArrayList<Book> books = bookRepository.search(keyword);
        ArrayList<BookDTO> result = new ArrayList<>();

        for (Book item: books) {
            result.add(BookMapper.INSTANCE.bookToBookDto(item));
        }
        return result;
    }

    public Optional<Book> getBook(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public List<Book> findAllAvailable() {
        return bookRepository.findAllAvailable();
    }
}
