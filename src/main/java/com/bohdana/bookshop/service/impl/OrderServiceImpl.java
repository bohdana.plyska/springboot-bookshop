package com.bohdana.bookshop.service.impl;

import com.bohdana.bookshop.dto.OrderDTO;
import com.bohdana.bookshop.entity.Order;
import com.bohdana.bookshop.entity.OrderItems;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.mappers.OrderMapper;
import com.bohdana.bookshop.mappers.UserMapper;
import com.bohdana.bookshop.repository.OrderItemsRepository;
import com.bohdana.bookshop.repository.OrderRepository;
import com.bohdana.bookshop.service.BookService;
import com.bohdana.bookshop.service.OrderService;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final OrderItemsRepository orderItemsRepository;

    private final UserService userService;

    private final BookService bookService;

    @Override
    public OrderDTO createOrder(Long userId) {
            User user = UserMapper.INSTANCE.userDtoToUser(userService.findById(userId));
            List<OrderItems> orderItems = orderItemsRepository.findUnpinnedUserOrderDetails(user.getId());
            double price = 0.0;

            for (OrderItems item : orderItems) {
                price += item.getPrice() * item.getBookAmount();
            }


            for (OrderItems item : orderItems) {
                bookService.updateBookAmount(item.getBookId().getId(), item.getBookAmount());
            }

            Order order = new Order();
            order.setUser(user);
            order.setOrderItems(orderItems);
            order.setPrice(price);

            for (OrderItems item : orderItems) {
                item.setPinned(true);
                orderItemsRepository.save(item);
            }
            return OrderMapper.INSTANCE.orderToOrderDto(orderRepository.save(order));

    }

    @Override
    public List<Order> getOrdersByUserId(Long userId) {
        return orderRepository.getOrdersByUserId(userId);
    }

    @Override
    public List<Order> listAllOrders() {
       return (List<Order>) orderRepository.findAll();
    }

}
