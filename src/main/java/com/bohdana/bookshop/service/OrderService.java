package com.bohdana.bookshop.service;

import com.bohdana.bookshop.dto.OrderDTO;
import com.bohdana.bookshop.entity.Order;

import java.util.List;

public interface OrderService {

    OrderDTO createOrder(Long userId);

    List<Order> getOrdersByUserId(Long userId);

    List<Order> listAllOrders();

}
