package com.bohdana.bookshop.controller;

import com.bohdana.bookshop.dto.OrderItemsDTO;
import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.OrderItems;
import com.bohdana.bookshop.service.OrderItemsService;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Controller
@RequiredArgsConstructor
public class  OrderItemsController {

    private final OrderItemsService orderItemsService;
    private final UserService userService;

    @GetMapping(ORDER_ITEMS_PAGE_URL)
    public ResponseEntity<List<OrderItems>> showBasket(Authentication authentication) {
        UserDTO user = userService.findByUsername(authentication.getName());
        List<OrderItems> list = orderItemsService.getOrderItemsForBasket(user.getId());
        return ResponseEntity.ok(list);
    }

    @PostMapping(ORDER_ITEMS_INCREASE_PAGE_URL)
    public ResponseEntity<Boolean> increaseAmount(@PathVariable Long id){
         orderItemsService.increaseAmount(id);
         return new ResponseEntity<>(true,HttpStatus.OK);
    }

    @PostMapping(ORDER_ITEMS_DECREASE_PAGE_URL)
    public ResponseEntity<Boolean> decreaseAmount(@PathVariable Long id){
        orderItemsService.decreaseAmount(id);
        return new ResponseEntity<>(true,HttpStatus.OK);
    }

    @DeleteMapping(ORDER_ITEMS_DELETE_PAGE_URL)
    public ResponseEntity<Boolean> delete(@PathVariable Long id) {
        orderItemsService.delete(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    // simple create
    @PostMapping(ORDER_ITEMS_ADD_TO_BASKET_PAGE_URL)
    public ResponseEntity<OrderItems> addOrderItemsToOrder (@PathVariable Long bookId, Authentication authentication) {
        UserDTO user = userService.findByUsername(authentication.getName());
        OrderItemsDTO orderItemsDTO = new OrderItemsDTO(bookId,user.getId());
        return new ResponseEntity<>(orderItemsService.addOrderItems(orderItemsDTO), HttpStatus.CREATED);
    }
}
