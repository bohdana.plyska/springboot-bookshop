package com.bohdana.bookshop.controller;

import com.bohdana.bookshop.dto.OrderDTO;
import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.Order;
import com.bohdana.bookshop.service.OrderService;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


import java.util.List;

import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Controller
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final UserService userService;

    @PostMapping(ORDER_CREATE_PAGE_URL)
    public ResponseEntity<OrderDTO> createOrder (Authentication authentication) {
        UserDTO user = userService.findByUsername(authentication.getName());
        return new ResponseEntity<>(orderService.createOrder(user.getId()), HttpStatus.CREATED);
    }

    @GetMapping(ORDER_ORDERS_PAGE_URL)
    public ResponseEntity<List<Order>> showOrders(Authentication authentication) {
        UserDTO user = userService.findByUsername(authentication.getName());
        List<Order> list = orderService.getOrdersByUserId(user.getId());
        return ResponseEntity.ok(list);
    }

    @GetMapping(ORDER_ORDERS_ALL_PAGE_URL)
    public  ResponseEntity<List<Order>> allOrders() {
        List<Order> list =orderService.listAllOrders();
        return ResponseEntity.ok(list);
    }

}
