package com.bohdana.bookshop.controller;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.mappers.UserMapper;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import static com.bohdana.bookshop.constants.DefaultAppConstants.SIGNUP_PAGE_URL;

@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    private static final Logger LOGGER = LogManager.getLogger(RegistrationController.class);

    @PostMapping(SIGNUP_PAGE_URL)
    public ResponseEntity<UserDTO> signUpUser (@RequestBody UserDTO userDTO) {
        if (userService.getUserByName(userDTO.getUsername()) != null) {
            LOGGER.error("username already exists");
        }
        if(userDTO.getPassword().length() < 7) {
            LOGGER.error("password too small");
            return ResponseEntity.badRequest().build();
        }
        User user = userService.save(userDTO);
        UserDTO savedUser = UserMapper.INSTANCE.userToUserDto(user);
        LOGGER.info("User created successfully");
        return ResponseEntity.ok(savedUser);
    }

}
