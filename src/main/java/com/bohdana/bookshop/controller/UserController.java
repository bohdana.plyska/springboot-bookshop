package com.bohdana.bookshop.controller;

import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.entity.User;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @GetMapping(USERS_PAGE_URL)
    public ResponseEntity<List<User>> showAllUsers() {
        List<User> users = new ArrayList<>();
        userService.listAllUsers().forEach(users::add);
        return ResponseEntity.ok(users);
    }

    @DeleteMapping(USER_DELETE_PAGE_URL)
    public ResponseEntity<Boolean> deleteUser(@PathVariable Long id) {
        userService.delete(id);
        if (userService.getUser(id).isPresent()) {
            LOGGER.error("An error occurs during deleting the user");
            return new ResponseEntity<>(false,HttpStatus.BAD_REQUEST);
        } else {
            LOGGER.info("The user was deleted");
            return new ResponseEntity<>(true,HttpStatus.OK);
        }
    }

    @GetMapping(USER_FIND_USER_BY_ID_URL)
    public ResponseEntity<UserDTO> findById(@PathVariable Long id) {
        return new ResponseEntity<>(userService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/users/findByUserName/{userName}")
    public ResponseEntity<UserDTO> findByUserName(@PathVariable String userName) {
        return new ResponseEntity<>(userService.findByUsername(userName), HttpStatus.OK);
    }

    @PostMapping(USER_UPDATE_PAGE_URL)
    public ResponseEntity<User> updateUser(@Valid UserDTO userDTO) {
        return new ResponseEntity<>(userService.update(userDTO),HttpStatus.OK);
    }

    @GetMapping(USER_SEARCH_PAGE_URL)
    public  ResponseEntity<ArrayList<UserDTO>> searchUser(@PathVariable String keyword) {
        ArrayList<UserDTO> searchResult = userService.search(keyword);
        return ResponseEntity.ok(searchResult);
    }

    @PostMapping(USER_SAVE_USER_PAGE_URL)
    public ResponseEntity<User> saveUser(@RequestBody UserDTO user) {
        return  new ResponseEntity<>( userService.save(user), HttpStatus.CREATED);
    }

}
