package com.bohdana.bookshop.controller;

import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.entity.Book;
import com.bohdana.bookshop.entity.GenreEnum;
import com.bohdana.bookshop.service.BookService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Controller
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    @PostMapping(BOOK_SAVE_PAGE_URL)
    public ResponseEntity<Book> saveAndUpdateBook(@RequestBody Book book) {
        return  new ResponseEntity<>(bookService.save(book),HttpStatus.CREATED);
    }

    @GetMapping(BOOKS_PAGE_URL)
    public ResponseEntity<List<Book>> showAllBooks() {
        List<Book> books = new ArrayList<>();
        bookService.listAllBooks().forEach(books::add);
        return ResponseEntity.ok(books);
    }

    @GetMapping(BOOK_SEARCH_PAGE_URL)
    public ResponseEntity<ArrayList<BookDTO>> searchBook(@PathVariable String keyword) {
        ArrayList<BookDTO> searchResult = bookService.search(keyword);
        return ResponseEntity.ok(searchResult);
    }

    @GetMapping(BOOK_FIND_ALL_AVAILABLE_PAGE_URL)
    public ResponseEntity<List<Book>> findAllAvailable() {
        List<Book> books = new ArrayList<>();
        bookService.findAllAvailable().forEach(books::add);
        return ResponseEntity.ok(books);
    }

    @DeleteMapping( BOOK_DELETE_PAGE_URL)
    public ResponseEntity<Boolean> deleteBook(@PathVariable Long id){
        bookService.delete(id);
        if (bookService.getBook(id).isPresent()){
            LOGGER.error("An error occurs during deleting the book");
            return new ResponseEntity<>(false,HttpStatus.BAD_REQUEST);
        } else {
            LOGGER.info("The book was deleted");
            return new ResponseEntity<>(true,HttpStatus.OK);
        }
    }
    @GetMapping(BOOK_FIND_BOOK_BY_ID_URL)
    public ResponseEntity<BookDTO> findById(@PathVariable Long id) {
        return new ResponseEntity<>(bookService.findById(id), HttpStatus.OK);
    }

    @GetMapping(BOOK_GENRES_PAGE_URL)
   public ResponseEntity<List<GenreEnum>> addGenresToModel(){
        return new ResponseEntity<>(Arrays.asList(GenreEnum.values()), HttpStatus.OK);
    }

}
