package com.bohdana.bookshop.validator;

import com.bohdana.bookshop.constants.DefaultAppConstants;
import com.bohdana.bookshop.dto.BookDTO;
import com.bohdana.bookshop.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Component
@RequiredArgsConstructor
public class BookValidator implements Validator {

    private final BookService bookService;

    @Override
    public boolean supports(Class<?> aClass) {
        return BookDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BookDTO book = (BookDTO) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_BOOK_NAME, NOT_EMPTY);
        if (book.getName().length() < MIN_USERNAME_FIELD_VALUE || book.getName().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_BOOK_NAME, "Size.bookForm.bookName");
        }

        if (bookService.findByName(book.getName()) != null) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_BOOK_NAME, "Duplicate.bookForm.bookName");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_PUBLISHER_NAME, NOT_EMPTY);
        if (book.getPublisherName().length() < MIN_PUBLISHER_NAME_FIELD_VALUE || book.getPublisherName().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_PUBLISHER_NAME, "Size.bookForm.publisherName");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_BOOK_AUTHOR_NAME, NOT_EMPTY);
        if (book.getAuthorName().length() < MIN_AUTHOR_NAME_FIELD_VALUE || book.getAuthorName().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_BOOK_AUTHOR_NAME, "Size.bookForm.authorName");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_BOOK_TITLE_NAME, NOT_EMPTY);
        if (book.getTitle().length() < MIN_TITLE_FIELD_VALUE || book.getAuthorName().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_BOOK_TITLE_NAME, "Size.bookForm.title");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_BOOK_PRICE, NOT_EMPTY);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_BOOK_NUMBER_OF_PAGES, NOT_EMPTY);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_BOOK_AMOUNT, NOT_EMPTY);

    }
}
