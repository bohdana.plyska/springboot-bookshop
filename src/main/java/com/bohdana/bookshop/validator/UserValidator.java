package com.bohdana.bookshop.validator;

import com.bohdana.bookshop.constants.DefaultAppConstants;
import com.bohdana.bookshop.dto.UserDTO;
import com.bohdana.bookshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.bohdana.bookshop.constants.DefaultAppConstants.*;

@Component
@RequiredArgsConstructor
public class UserValidator implements Validator {

    private final UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_USERNAME, NOT_EMPTY);
        if (user.getUsername().length() < MIN_USERNAME_FIELD_VALUE || user.getUsername().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_USERNAME, "Size.userForm.username");
        }
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_USERNAME, "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_EMAIL, NOT_EMPTY);
        if (user.getEmail().length() < MIN_EMAIL_FIELD_VALUE || user.getEmail().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_EMAIL, "Size.userForm.email");
        }
        if (userService.findByEmail(user.getEmail()) != null) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_EMAIL, "Duplicate.userForm.email");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, DefaultAppConstants.VALIDATION_FIELD_PASSWORD, NOT_EMPTY);
        if (user.getPassword().length() < MIN_PASSWORD_FIELD_VALUE || user.getPassword().length() > MAX_FIELD_VALUE) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_PASSWORD, "Size.userForm.password");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue(DefaultAppConstants.VALIDATION_FIELD_PASSWORD_CONFIRM, "Diff.userForm.passwordConfirm");
        }
    }
}
