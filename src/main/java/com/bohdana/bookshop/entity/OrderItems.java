package com.bohdana.bookshop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "orderItems")
@Data
@NoArgsConstructor
@DynamicUpdate
@AllArgsConstructor
public class OrderItems {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "BOOK_AMOUNT")
    private Integer bookAmount;

    @Column(name= "PRICE")
    private Float price;

    @Column(name="PINNED")
    private boolean pinned;

    @ManyToOne(optional = true)
    @JoinColumn(name="order_id", nullable = true, updatable = true)
    @JsonIgnore
    private Order orderId;

    @ManyToOne
    @JoinColumn(name="bookId")
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Book bookId ;

    @ManyToOne
    @JoinColumn(name="userId", nullable = false)
    @JsonIgnore
    private User userId;

    public OrderItems( Book bookId, Integer bookAmount, User userId, Float price) {
        this.bookId = bookId;
        this.bookAmount = bookAmount;
        this.userId = userId;
        this.price = price;
        this.pinned = false;

    }

    public OrderItems(Book bookId, User userId) {
        this.bookId = bookId;
        this.userId = userId;
    }

    public OrderItems(Long id, Integer bookAmount, Float price, boolean pinned) {
        this.id = id;
        this.bookAmount = bookAmount;
        this.price = price;
        this.pinned = pinned;
    }

}
