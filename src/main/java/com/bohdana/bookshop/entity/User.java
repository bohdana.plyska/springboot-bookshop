package com.bohdana.bookshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "USER_NAME", nullable = false)
    @Size(min = 4, message = "at least 4 characters")
    private String username;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @NotNull
    @Column(name = "EMAIL", nullable = false)
    @Size(min = 4, message = "at least 4 characters")
    private String email;

    @NotNull
    @Column(name = "PASSWORD", nullable = false)
    @Size(min = 8, message = "at least 8 characters")
    private String password;

    @Transient
    private String passwordConfirm;

    @Column(name = "CITY")
    private String city;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "STREET_ADDRESS")
    private String streetAddress;

    @Column(name = "ZIP_CODE")
    private String zipcode;

    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;

    @Column(name = "ACTIVE")
    private boolean active;

    @Column(name = "SALT")
    private String salt;

    @Enumerated(EnumType.STRING)
    private Role role;

    public User(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

}
