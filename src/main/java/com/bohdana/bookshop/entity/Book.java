package com.bohdana.bookshop.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name ="books")
@Data
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    @Size(min = 4, message = "at least 4 characters")
    private String name;

    @Column(name = "PUBLISHER_NAME",nullable = false)
    @Size(min = 4, message = "at least 4 characters")
    private String publisherName;

    @Column(name = "AUTHOR_NAME", nullable = false)
    @Size(min = 4, message = "at least 4 characters")
    private String authorName;

    @Column(name = "TITLE", nullable = false)
    @Size(min = 4, message = "at least 4 characters")
    private String title;

    @Column(name = "PRICE", nullable = false)
    private Float price;

    @Column(name = "NUMBER_OF_PAGES")
    private Long numberOfPages;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "BOOK_TYPE")
    private GenreEnum bookType;

    public Book(Long id, String name, String publisherName,
                String authorName, String title, Float price,
                Long numberOfPages, Long amount, GenreEnum bookType) {
        this.id = id;
        this.name = name;
        this.publisherName = publisherName;
        this.authorName = authorName;
        this.title = title;
        this.price = price;
        this.numberOfPages = numberOfPages;
        this.amount = amount;
        this.bookType = bookType;
    }
}

