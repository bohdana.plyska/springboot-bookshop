package com.bohdana.bookshop.entity;

import java.util.Arrays;

public enum GenreEnum {
    Fantasy("Fantasy"),
    Adventure("Adventure"),
    Romance("Romance"),
    Mystery("Mystery"),
    Horror("Horror"),
    Thriller("Thriller"),
    Paranormal("Paranormal");

    private final String name;

    GenreEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static GenreEnum getValueByString(String s) {
        return Arrays.stream(GenreEnum.values())
                .filter(genreValue -> s.equals(genreValue.getName()))
                .findFirst()
                .orElse(null);
    }
}

