package com.bohdana.bookshop.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "orders")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "PRICE")
    private double price;

    @Column(name = "HANDLED")
    private boolean handled;

    @ManyToOne
    private User user;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "orderId", orphanRemoval = true)
    private List<OrderItems> orderItems;

    public Order(Long id, double price, boolean handled, User user) {
        this.id = id;
        this.price = price;
        this.handled = handled;
        this.user = user;
    }
}
