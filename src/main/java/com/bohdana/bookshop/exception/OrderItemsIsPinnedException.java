package com.bohdana.bookshop.exception;

public class OrderItemsIsPinnedException extends RuntimeException {
    public OrderItemsIsPinnedException(String message) {
        super(message);
    }

}
