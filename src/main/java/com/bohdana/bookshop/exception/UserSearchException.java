package com.bohdana.bookshop.exception;

public class UserSearchException extends RuntimeException{
    public UserSearchException(String message) {
        super(message);
    }

}
