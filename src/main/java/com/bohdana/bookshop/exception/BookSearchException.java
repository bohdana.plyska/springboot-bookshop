package com.bohdana.bookshop.exception;

public class BookSearchException extends RuntimeException{
    public BookSearchException(String message) {
        super(message);
    }

}
