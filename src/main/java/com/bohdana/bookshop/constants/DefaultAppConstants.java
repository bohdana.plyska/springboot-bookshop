package com.bohdana.bookshop.constants;

public class DefaultAppConstants {

	/* Group of constants for page URL */
	public static final String SIGNUP_PAGE_URL = "/signup";
	public static final String USERS_PAGE_URL = "/users";
	public static final String USER_DELETE_PAGE_URL = "/users/delete/{id}";
	public static final String USER_UPDATE_PAGE_URL = "/users/edit";
	public static final String USER_SEARCH_PAGE_URL ="/searchUser/{keyword}";
	public static final String USER_SAVE_USER_PAGE_URL ="/save";
	public static final String USER_FIND_USER_BY_ID_URL ="/users/{id}";
	public static final String BOOK_FIND_ALL_AVAILABLE_PAGE_URL ="/findAllAvailable";
	public static final String BOOK_SEARCH_PAGE_URL ="/searchBook/{keyword}";
	public static final String BOOK_SAVE_PAGE_URL ="/saveBook";
	public static final String BOOK_FIND_BOOK_BY_ID_URL ="/books/{id}";
	public static final String BOOKS_PAGE_URL ="/books";
	public static final String BOOK_DELETE_PAGE_URL ="/books/delete/{id}";
	public static final String BOOK_GENRES_PAGE_URL ="/genres";
	public static final String ORDER_ITEMS_PAGE_URL ="/showBasket";
	public static final String ORDER_ITEMS_INCREASE_PAGE_URL ="/increaseAmount/{id}";
	public static final String ORDER_ITEMS_DECREASE_PAGE_URL ="/decreaseAmount/{id}";
	public static final String ORDER_ITEMS_DELETE_PAGE_URL ="/deleteOrderItem/{id}";
	public static final String ORDER_ITEMS_ADD_TO_BASKET_PAGE_URL ="/addToBasket/{bookId}";
	public static final String ORDER_CREATE_PAGE_URL ="/createOrder";
	public static final String ORDER_ORDERS_PAGE_URL ="/showOrders";
	public static final String ORDER_ORDERS_ALL_PAGE_URL ="/showAllOrders";

	/* Group of constants for Validation */
	public static final String VALIDATION_FIELD_USERNAME = "username";
	public static final String VALIDATION_FIELD_EMAIL = "email";
	public static final String VALIDATION_FIELD_PASSWORD = "password";
	public static final String VALIDATION_FIELD_PASSWORD_CONFIRM = "passwordConfirm";
	public static final Integer MAX_FIELD_VALUE = 32;
	public static final Integer MIN_USERNAME_FIELD_VALUE = 4;
	public static final Integer MIN_EMAIL_FIELD_VALUE = 4;
	public static final Integer MIN_PASSWORD_FIELD_VALUE = 8;
	public static final String NOT_EMPTY = "NotEmpty";
	public static final String VALIDATION_FIELD_BOOK_NAME = "name";
	public static final String VALIDATION_FIELD_PUBLISHER_NAME = "publisherName";
	public static final Integer MIN_PUBLISHER_NAME_FIELD_VALUE = 4;
	public static final String VALIDATION_FIELD_BOOK_AUTHOR_NAME = "authorName";
	public static final Integer MIN_AUTHOR_NAME_FIELD_VALUE = 4;
	public static final String VALIDATION_FIELD_BOOK_TITLE_NAME = "title";
	public static final Integer MIN_TITLE_FIELD_VALUE = 4;
	public static final String VALIDATION_FIELD_BOOK_PRICE = "price";
	public static final String VALIDATION_FIELD_BOOK_NUMBER_OF_PAGES = "numberOfPages";
	public static final String VALIDATION_FIELD_BOOK_AMOUNT ="amount";

	/* Group of constants for creating default administrator user in DB, when starting for the first time */
	public static final String DEFAULT_USER_NAME_ADMIN = "admin";
	public static final String DEFAULT_USER_PASSWORD_FOR_ADMIN = "1";
	public static final Long DEFAULT_USER_ADMIN_ID = 1L;
	public static final String DEFAULT_USER_ADMIN_EMAIL = "admin@admin.com";

	public static final String CONSTANT_CLASS = "Constant class";

	private DefaultAppConstants() {
		throw new IllegalStateException(CONSTANT_CLASS);
	}
}
